﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Interfaces.Services;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Services
{
    public class RESTfulMapsService : IRESTfulMapsService
    {
        public RESTfulMapsService(IRoadMatchService roadMatchService, IRailRoutingService railRoutingService,
            ISnapLocationService snapLocationService)
        {
            RoadMatchService = roadMatchService;
            RailRoutingService = railRoutingService;
            SnapLocationService = snapLocationService;
        }

        private IRoadMatchService RoadMatchService { get; set; }

        private IRailRoutingService RailRoutingService { get; set; }


        private ISnapLocationService SnapLocationService { get; set; }

        [LoggingMethod]
        public IEnumerable<Coordinate> GetRailRoute(Coordinate startingLocation, Coordinate destinationLocation)
        {
            return RailRoutingService.GetRailRoute(startingLocation, destinationLocation);
        }

        public RoadDetails GetRoadMatch(Coordinate queryLocation)
        {
            return RoadMatchService.GetRoadMatch(queryLocation);
        }

        public RoadDetails GetRoadMatch(Coordinate queryLocation, double bearing)
        {
            return RoadMatchService.GetRoadMatch(queryLocation, bearing);
        }


        public Int32 GetSpeedLimit(Coordinate queryLocation)
        {
            RoadDetails details = RoadMatchService.GetRoadMatch(queryLocation);
            return Decimal.ToInt32(details.SpeedLimit);
        }
    }
}
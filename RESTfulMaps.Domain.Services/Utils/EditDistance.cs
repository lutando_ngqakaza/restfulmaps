﻿using System;

namespace RESTfulMapsGateway.Domain.Services.Utils
{
    public class EditDistance
    {
        public static int LevenshteinDistance(string source, string target)
        {
            if (String.IsNullOrEmpty(source))
            {
                if (String.IsNullOrEmpty(target)) return 0;
                return target.Length;
            }
            if (String.IsNullOrEmpty(target)) return source.Length;

            if (source.Length > target.Length)
            {
                string temp = target;
                target = source;
                source = temp;
            }
            target = target.ToLower();
            source = source.ToLower();

            int m = target.Length;
            int n = source.Length;
            var distance = new int[2, m + 1];
            // Initialize the distance 'matrix'
            for (int j = 1; j <= m; j++) distance[0, j] = j;

            int currentRow = 0;
            for (int i = 1; i <= n; ++i)
            {
                currentRow = i & 1;
                distance[currentRow, 0] = i;
                int previousRow = currentRow ^ 1;
                for (int j = 1; j <= m; j++)
                {
                    int cost = (target[j - 1] == source[i - 1] ? 0 : 1);
                    distance[currentRow, j] = Math.Min(Math.Min(
                        distance[previousRow, j] + 1,
                        distance[currentRow, j - 1] + 1),
                        distance[previousRow, j - 1] + cost);
                }
            }
            int ret = distance[currentRow, m];
            return ret;
            //return distance[currentRow, m];
        }

        public static int ScaledLevenshteinDistance(string source, string target, int distance)
        {
            int editDistance = LevenshteinDistance(source, target);
            double scaledDistance = 1;
            double scaleFactor = 1;
            double stretchFactor = 1;


            stretchFactor = Math.Pow(Math.E, editDistance);

            scaleFactor = Math.Pow(Math.E, distance/1000);

            scaledDistance = editDistance*scaleFactor*stretchFactor;

            return (int) scaledDistance;
        }
    }
}
﻿using System;

namespace RESTfulMapsGateway.Domain.Services.Utils
{
    public static class Calculator
    {
        /// <summary>
        ///     Calculates the distance between two points of latitude and longitude.
        ///     Great Link - http://www.movable-type.co.uk/scripts/latlong.html
        /// </summary>
        /// <param name="lat1">First coordinate.</param>
        /// <param name="long1">First coordinate.</param>
        /// <param name="lat2">Second coordinate.</param>
        /// <param name="long2">Second coordinate.</param>
        /// <returns>the distance in metres</returns>
        public static Double DistanceInMetres(double lat1, double lon1, double lat2, double lon2)
        {
            if (lat1 == lat2 && lon1 == lon2)
                return 0.0;

            double theta = lon1 - lon2;

            double distance = Math.Sin(deg2rad(lat1))*Math.Sin(deg2rad(lat2)) +
                              Math.Cos(deg2rad(lat1))*Math.Cos(deg2rad(lat2))*
                              Math.Cos(deg2rad(theta));

            distance = Math.Acos(distance);
            if (double.IsNaN(distance))
                return 0.0;

            distance = rad2deg(distance);
            distance = distance*60.0*1.1515*1609.344;

            return Math.Round(distance, 2);
        }

        /// <summary>
        ///     Overload for decimals
        /// </summary>
        /// <returns></returns>
        public static Double DistanceInMetres(decimal lat1, decimal lon1, decimal lat2, decimal lon2)
        {
            return DistanceInMetres((double) (lat1), (double) (lon1), (double) (lat2), (double) (lon2));
        }

        public static Double DistancePointToSegmentInMeters(decimal px, decimal py, decimal x1, decimal y1, decimal x2,
            decimal y2, bool onlyLeft)
        {
            decimal lineMagnitude = DistanceMagnitudeSquared(x1, x2, y1, y2);

            if (lineMagnitude < 0.00000001m)
            {
                //approximate the segment as a point.
                return DistanceInMetres(px, py, x1, y1);
            }
            if (onlyLeft)
            {
                double angle = VectorAngle(x2 - x1, y2 - y1, px, py);
                if (angle > 180)
                {
                    return 999999;
                }
            }

            //Geometry hax that I don't really bother to understand.
            decimal u1 = (((px - x1)*(x2 - x1)) + ((py - y1)*(y2 - y1)));
            decimal u = u1/lineMagnitude;

            if (u < 0.00001m || u > 1)
            {
                // closest point does not fall within the line segment, take the shorter distance to an endpoint

                double ix = DistanceInMetres(px, py, x1, y1);
                double iy = DistanceInMetres(px, py, x2, y2);
                return Math.Min(ix, iy);
            }

            else
            {
                //Intersecting point is on the line, use MOAR HAX
                decimal ix = x1 + u*(x2 - x1);
                decimal iy = y1 + u*(y2 - y1);
                return DistanceInMetres(px, py, ix, iy);
            }
        }

        public static decimal DistanceMagnitudeSquared(decimal lat1, decimal lon1, decimal lat2, decimal lon2)
        {
            return ((lat1 - lat2)*(lat1 - lat2) + (lon1 - lon2)*(lon1 - lon2));
        }

        public static decimal DecimalDotProduct(decimal lat1, decimal lon1, decimal lat2, decimal lon2)
        {
            return (lat1*lat2) + (lon1*lon2);
        }

        public static double VectorAngle(decimal lat1, decimal lon1, decimal lat2, decimal lon2)
        {
            decimal dotProd = DecimalDotProduct(lat1, lon1, lat2, lon2);
            double lenProd = Math.Sqrt((double) DistanceMagnitudeSquared(lat1, lon1, lat2, lon2));
            double divOperation = (double) dotProd/lenProd;
            return Math.Acos(divOperation)*(180.0/Math.PI);
        }

        private static double deg2rad(double deg)
        {
            return (deg*Math.PI/180.0);
        }

        private static double rad2deg(double rad)
        {
            return (rad/Math.PI*180.0);
        }
    }
}
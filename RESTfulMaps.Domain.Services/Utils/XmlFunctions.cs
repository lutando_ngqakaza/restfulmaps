﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace RESTfulMapsGateway.Domain.Services.Utils
{
    public static class XmlFunctions
    {
        [SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes",
            MessageId = "System.Xml.XmlNode")]
        public static XmlNode FindNode(XmlNodeList list, string nodeName)
        {
            if (list.Count > 0)
            {
                foreach (XmlNode node in list)
                {
                    if (node.LocalName.Equals(nodeName)) return node;
                    if (node.HasChildNodes)
                    {
                        XmlNode nodeFound = FindNode(node.ChildNodes, nodeName);
                        if (nodeFound != null)
                            return nodeFound;
                    }
                }
            }
            return null;
        }

        public static string Serialize<T>(T value)
        {
            string serializeXml = "Unable to serialize";
            if (value == null)
            {
                return serializeXml;
            }
            try
            {
                var xmlSerializer = new XmlSerializer(typeof (T));
                var stringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(stringWriter);

                xmlSerializer.Serialize(writer, value);

                serializeXml = stringWriter.ToString();

                writer.Close();
                return serializeXml;
            }
            catch (Exception)
            {
                return serializeXml;
            }
        }

        public static string Serialize(object value)
        {
            string serializeXml = "Unable to serialize";
            if (value == null)
            {
                return serializeXml;
            }
            try
            {
                var xmlSerializer = new XmlSerializer(value.GetType());
                var stringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(stringWriter);

                xmlSerializer.Serialize(writer, value);

                serializeXml = stringWriter.ToString();

                writer.Close();
                return serializeXml;
            }
            catch (Exception)
            {
                return serializeXml;
            }
        }

        public static string SerializeDataContract<T>(T value)
        {
            string serializeXml = "Unable to serialize";

            if (value == null)
            {
                return serializeXml;
            }
            try
            {
                var memStream = new MemoryStream();

                var serializer = new DataContractSerializer(typeof (T));
                serializer.WriteObject(memStream, value);

                memStream.Seek(0, SeekOrigin.Begin);

                using (var streamReader = new StreamReader(memStream))
                {
                    serializeXml = streamReader.ReadToEnd();
                    return serializeXml;
                }
            }
            catch (Exception)
            {
                return serializeXml;
            }
        }
    }
}
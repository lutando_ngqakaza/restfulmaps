﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Interfaces.Repositories;
using RESTfulMapsGateway.Domain.Interfaces.Services;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using RESTfulMapsGateway.Domain.Services.Utils;
using RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Services
{
    public class PointsOfInterestWrapperService : DisposingService, IPointsOfInterestWrapperService
    {
        public PointsOfInterestWrapperService(IPointsOfInterestService poiService, ITokenRepository tokenRepo)
        {
            PoiService = poiService;
            TokenRepository = tokenRepo;
        }

        public PointsOfInterestWrapperService()
        {
            PoiService = new DeCartaPointsOfInterestService();
        }

        protected IPointsOfInterestService PoiService { get; set; }

        protected ITokenRepository TokenRepository { get; set; }

        public async Task<IEnumerable<NearbyPointOfInterest>> GetNearbyPointsOfInterestAsync(string searchText,
            Coordinate location, int limit, DateTime atDate)
        {
            return
                await
                    Task.Factory.StartNew(() => GetNearbyPointsOfInterest(searchText, location, limit, atDate))
                        .ConfigureAwait(false);
        }

        public IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string searchText, Coordinate location,
            int limit, DateTime atDate)
        {
            IEnumerable<PointOfInterest> pois = PoiService.GetPointsOfInterest(searchText, location, limit);

            var returnList = new List<NearbyPointOfInterest>();

            foreach (PointOfInterest p in pois)
            {
               
                returnList.Add(
                    new NearbyPointOfInterest(
                        p,
                        (int)
                            Calculator.DistanceInMetres(location.Latitude, location.Longitude, p.Location.Latitude,
                                p.Location.Longitude)));
            }

            return
                returnList.OrderBy(
                    x => EditDistance.ScaledLevenshteinDistance(x.PointOfInterest.Name, searchText, x.DistanceInMeters))
                    .ThenBy(x => x.DistanceInMeters);
   
        }

        public IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string tokenValue, string searchText,
            int limit, DateTime atDate)
        {
            
            return GetNearbyPointsOfInterest(searchText, limit, atDate).ToList();
        }

        public IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string searchText, int limit,
            DateTime atDate)
        {
            IEnumerable<PointOfInterest> pois = PoiService.GetPointsOfInterest(searchText, limit);

            var returnList = new List<NearbyPointOfInterest>();

            foreach (PointOfInterest p in pois)
            {
               
                returnList.Add(
                    new NearbyPointOfInterest(p, 0));
            }


            return
                returnList.OrderBy(x => EditDistance.LevenshteinDistance(x.PointOfInterest.Name, searchText))
                    .ThenBy(x => x.PointOfInterest.Name);
        }

        public async Task<IEnumerable<NearbyPointOfInterest>> GetNearbyPointsOfInterestAsync(string searchText,
            int limit, DateTime atDate)
        {
            return
                await
                    Task.Factory.StartNew(() => GetNearbyPointsOfInterest(searchText, limit, atDate))
                        .ConfigureAwait(false);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
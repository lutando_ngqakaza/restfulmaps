﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace RESTfulMapsGateway.Domain.Services
{
    public abstract class DisposingService : IDisposable
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Trace.WriteLine("Disposing Service: " + GetType().FullName);

            if (disposing)
            {
                PropertyInfo[] properties =
                    GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                IEnumerable<IDisposable> disposables = properties.Select(x => x.GetValue(this)).OfType<IDisposable>();

                foreach (IDisposable disposable in disposables)
                {
                    Debug.WriteLine("Disposing " + disposable.GetType().FullName);
                    disposable.Dispose();
                }
            }
        }
    }
}
﻿using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.Enums;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IPathingService
    {
        RouteDetails GetRoute(ICollection<Coordinate> points, TransitMode mode);
    }
}
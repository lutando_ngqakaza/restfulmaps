﻿namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IConfigurationReader
    {
        string ReadConfigurationValue(string key);
    }
}
﻿using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IRoadMatchService
    {
        RoadDetails GetRoadMatch(Coordinate queryLocation);

        RoadDetails GetRoadMatch(Coordinate queryLocation, double bearing);
    }
}
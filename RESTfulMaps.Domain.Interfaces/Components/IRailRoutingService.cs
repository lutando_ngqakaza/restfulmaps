﻿using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IRailRoutingService
    {
        IEnumerable<Coordinate> GetRailRoute(Coordinate startingLocation, Coordinate destinationLocation);

        RouteDetails GetRailRouteDetails(Coordinate startingLocation, Coordinate destinationLocation);
    }
}
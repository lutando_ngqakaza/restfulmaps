﻿using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IRoadRoutingService
    {
        RouteDetails GetCarRouteDetails(ICollection<Coordinate> points);

        RouteDetails GetWalkingRouteDetails(ICollection<Coordinate> points);
    }
}
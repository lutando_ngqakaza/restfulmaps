﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IPointsOfInterestService
    {
        IEnumerable<PointOfInterest> GetPointsOfInterest(String searchText, int limit);

        IEnumerable<PointOfInterest> GetPointsOfInterest(String searchText, Coordinate searchCoordinate, int limit);
    }
}
﻿using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface IReverseGeocodeService
    {
        RoadDetails GetReverseGeocode(Coordinate queryLocation);
    }
}
﻿using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Components
{
    public interface ISnapLocationService
    {
        Coordinate GetSnappedLocation(Coordinate queryLocation);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Services
{
    public interface IPointsOfInterestWrapperService : IDisposable
    {
        Task<IEnumerable<NearbyPointOfInterest>> GetNearbyPointsOfInterestAsync(string searchText, Coordinate location,
            int limit, DateTime atDate);

        IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string searchText, Coordinate location, int limit,
            DateTime atDate);

        IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string tokenValue, string searchText, int limit,
            DateTime atDate);

        IEnumerable<NearbyPointOfInterest> GetNearbyPointsOfInterest(string searchText, int limit, DateTime atDate);

        Task<IEnumerable<NearbyPointOfInterest>> GetNearbyPointsOfInterestAsync(string searchText, int limit,
            DateTime atDate);
    }
}
﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Interfaces.Services
{
    public interface IRESTfulMapsService
    {
        IEnumerable<Coordinate> GetRailRoute(Coordinate startingLocation, Coordinate destinationLocation);

        RoadDetails GetRoadMatch(Coordinate queryLocation);

        RoadDetails GetRoadMatch(Coordinate queryLocation, double bearing);


        Int32 GetSpeedLimit(Coordinate queryLocation);
    }
}
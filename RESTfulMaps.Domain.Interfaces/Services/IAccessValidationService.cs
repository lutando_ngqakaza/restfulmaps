﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.Entities;
using RESTfulMapsGateway.Domain.Models.Enums;

namespace RESTfulMapsGateway.Domain.Interfaces.Services
{
    public interface IAccessValidationService : IDisposable
    {
        AppKeyState ValidateAppkey(string appkeyValue, IEnumerable<int> appDomains);

        TokenState ValidateToken(string tokenValue, IEnumerable<int> appDomains);

        Token GetTokenFromValue(Guid tokenValue);
    }
}
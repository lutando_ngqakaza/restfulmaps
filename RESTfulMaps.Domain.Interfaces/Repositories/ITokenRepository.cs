﻿using System;
using DomainDrivenArchitecture.Interfaces;
using RESTfulMapsGateway.Domain.Models.Entities;

namespace RESTfulMapsGateway.Domain.Interfaces.Repositories
{
    public interface ITokenRepository : IRepository<Token, Guid>
    {
        Token GetByValue(Guid tokenValue);
    }
}
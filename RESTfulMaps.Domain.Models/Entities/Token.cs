﻿using System;
using DomainDrivenArchitecture.Models;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.Entities
{
    public class Token : DomainEntity<Guid>, IAggregateRoot
    {
        public Token(Guid appKey, Guid userId)
            : base(CombIdentityFactory.GenerateIdentity())
        {
            AppKey = appKey;
            TokenValue = Guid.NewGuid();
            UserId = userId;
        }

        // Hydrate
        public Token(Guid id, Guid tokenValue, Guid appKey, Guid userId)
            : base(id)
        {
            AppKey = appKey;
            TokenValue = tokenValue;
            UserId = userId;
        }

        public Guid TokenValue { get; private set; }

        public Guid UserId { get; private set; }

        public Guid AppKey { get; private set; }

        public Coordinate AssociatedLocation { get; set; }

        // Instantiate
    }
}
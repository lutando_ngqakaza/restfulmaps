﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainDrivenArchitecture.Models;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.ValueObjects
{
    public class PointOfInterest : IValueObject<PointOfInterest>
    {
        private List<string> categories;
        private string name;

        public PointOfInterest(string name, string description, IEnumerable<string> categorylist, Coordinate location,
            string address)
        {
            Name = name;
            Description = description;
            Categories = categorylist;
            Location = location;
            Address = address;
        }

        public string Name
        {
            get { return name; }
            private set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentOutOfRangeException("value", "PointOfInterest name cannot be an empty value");
                }
                if (value.Length == 1)
                {
                    throw new ArgumentOutOfRangeException("value",
                        "PointOfInterest name must be more than one character");
                }

                name = value;
            }
        }

        public string Description { get; private set; }


        public IEnumerable<string> Categories
        {
            get { return categories; }
            private set
            {
                if (value == null || !value.Any())
                {
                    categories = new List<string>();
                }
                else
                {
                    categories = value.ToList();
                }
            }
        }

        public Coordinate Location { get; private set; }

        public string Address { get; private set; }


        public bool Equals(PointOfInterest other)
        {
            return (other != null && name == other.Name && Description == other.Description &&
                    categories.Any(x => other.categories.Any(x.Equals)) &&
                    categories.Count() == other.categories.Count() && Location.Equals(other.Location) &&
                    Address == other.Address);
        }
    }
}
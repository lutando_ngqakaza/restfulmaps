﻿using System.Collections.Generic;
using DomainDrivenArchitecture.Models;

namespace RESTfulMapsGateway.Domain.Models.ValueObjects
{
    public class NearbyPointOfInterest : IValueObject<NearbyPointOfInterest>
    {
        public NearbyPointOfInterest(PointOfInterest pointOfInterest, int distance)
        {
            PointOfInterest = pointOfInterest;
            DistanceInMeters = distance;
        }

        public PointOfInterest PointOfInterest { get; private set; }

        public int DistanceInMeters { get; private set; }




        public bool Equals(NearbyPointOfInterest other)
        {
            if (other == null)
            {
                return false;
            }

            var compare = new NearbyPointOfInterestComparer();

            return compare.Equals(this, other);
        }
    }

    public class NearbyPointOfInterestComparer : IEqualityComparer<NearbyPointOfInterest>
    {
        public bool Equals(NearbyPointOfInterest x, NearbyPointOfInterest y)
        {
            return x.PointOfInterest.Name.Equals(y.PointOfInterest.Name) && x.DistanceInMeters == y.DistanceInMeters;
        }

        public int GetHashCode(NearbyPointOfInterest obj)
        {
            return obj.PointOfInterest.GetHashCode()*13 + obj.DistanceInMeters.GetHashCode()*17;
        }
    }
}
﻿using System;
using DomainDrivenArchitecture.Models;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.ValueObjects
{
    public class TravelDistance : IValueObject<TravelDistance>
    {
        public TravelDistance(Coordinate start, Coordinate destination, decimal dist, String mode)
        {
            StartingLocation = start;
            DestinationLocation = destination;
            Distance = dist;
            TravelMode = mode;
        }

        public Coordinate StartingLocation { get; private set; }
        public Coordinate DestinationLocation { get; private set; }

        public decimal Distance { get; set; }

        public string TravelMode { get; private set; }


        public bool Equals(TravelDistance other)
        {
            return (other != null) && (Distance == other.Distance);
        }
    }
}
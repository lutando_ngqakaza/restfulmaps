﻿using System;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.ValueObjects
{
    public class RoadDetails
    {
        public RoadDetails(string province, string city, string suburb, string streetName, decimal speedLimit,
            Coordinate location)
        {
            StreetName = streetName;
            SpeedLimit = speedLimit;
            Location = location;
            Province = province;
            City = city;
            Suburb = suburb;
            Address = String.Format("{0}, {1}, {2}, {3}", StreetName, Suburb, City, Province).Trim(',').Trim();
        }

        public String StreetName { get; set; }

        public String Suburb { get; set; }

        public String City { get; set; }

        public String Province { get; set; }

        public Decimal SpeedLimit { get; set; }

        public Coordinate Location { get; set; }

        public String Address { get; set; }
    }
}
﻿namespace RESTfulMapsGateway.Domain.Models.Enums
{
    public enum TokenState
    {
        Uninitialised,
        Valid,
        NotAGuid,
        NotFound,
        UserNotFound,
        UserInactive,
        UserRateLimitExceeded,
        InvalidAppKey,
        OutOfDateAppKey,
        IncorrectDomainAppKey
    };
}
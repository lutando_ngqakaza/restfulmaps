﻿namespace RESTfulMapsGateway.Domain.Models.Enums
{
    public enum AppKeyState
    {
        Uninitialised,
        Valid,
        NotAGuid,
        NotFound,
        Inactive,
        OutOfDate,
        IncorrectDomain
    }
}
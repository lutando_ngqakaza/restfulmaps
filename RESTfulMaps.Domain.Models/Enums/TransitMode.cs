﻿namespace RESTfulMapsGateway.Domain.Models.Enums
{
    public enum TransitMode
    {
        Driving = 1,
        Walking = 2,
        Rail = 3
    }
}
﻿namespace RESTfulMapsGateway.Domain.Models.Enums
{
    public enum InputModelType
    {
        IndividualTravelDistance,
        MultipleTravelDistance,
    }
}
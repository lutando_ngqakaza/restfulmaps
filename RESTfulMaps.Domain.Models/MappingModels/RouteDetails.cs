﻿using System;
using System.Collections.Generic;
using System.Linq;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.RESTfulMapsModels
{
    public class RouteDetails
    {
        public RouteDetails(ICollection<Coordinate> shape, int duration, int distance)
        {
            Shape = new List<String>(shape.Select(x => x.ToString()));
            Duration = duration;
            Distance = distance;
        }

        public RouteDetails(ICollection<Coordinate> shape, ICollection<Directions> instructions, int duration,
            int distance)
        {
            Shape = new List<String>(shape.Select(x => x.ToString()));
            Instructions = instructions;
            Duration = duration;
            Distance = distance;
        }

        public Int32 Duration { get; set; }

        public Int32 Distance { get; set; }

        public ICollection<String> Shape { get; private set; }

        public ICollection<Directions> Instructions { get; private set; }
    }
}
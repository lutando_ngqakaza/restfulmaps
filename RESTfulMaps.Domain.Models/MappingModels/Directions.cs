﻿using System;
using System.Collections.Generic;
using System.Linq;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Domain.Models.RESTfulMapsModels
{
    /// <summary>
    ///     The directions that describe a path
    /// </summary>
    public class Directions
    {
        public Directions(string instruction, int duration, int distance, ICollection<Coordinate> directionShape)
        {
            Instruction = instruction;
            Duration = duration;
            Distance = distance;
            DirectionShape = new List<String>(directionShape.Select(x => x.ToString()));
        }

        /// <summary>
        ///     A descriptive instruction on how to navigate the path.
        /// </summary>
        public String Instruction { get; set; }

        /// <summary>
        ///     The duration of the direction
        /// </summary>
        public Int32 Duration { get; set; }

        /// <summary>
        ///     The distance of the direction
        /// </summary>
        public Int32 Distance { get; set; }

        /// <summary>
        ///     The series of points describing the direction
        /// </summary>
        public ICollection<String> DirectionShape { get; private set; }
    }
}
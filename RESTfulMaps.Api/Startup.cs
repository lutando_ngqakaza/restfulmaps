﻿using System.Web.Http;
using RESTfulMapsGateway.Api.App_Start;
using Owin;

//[assembly: OwinStartup(typeof(RESTfulMapsGateway.Api.Startup))]

namespace RESTfulMapsGateway.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            WebApiConfig.Register(config);
            SwaggerConfig.Register(config);


            app.UseWebApi(config);
        }
    }
}
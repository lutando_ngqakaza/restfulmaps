﻿using System;
using RESTfulMapsGateway.Domain.Models.Enums;

namespace RESTfulMapsGateway.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public sealed class AppDomainAttribute : Attribute
    {
        private readonly AppDomainCall appDomain;

        public AppDomainAttribute(AppDomainCall appDomain)
        {
            this.appDomain = appDomain;
        }

        public AppDomainCall AppDomain
        {
            get { return appDomain; }
        }
    }
}
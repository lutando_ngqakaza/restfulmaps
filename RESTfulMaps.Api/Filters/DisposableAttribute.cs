﻿using System;

namespace RESTfulMapsGateway.Api.Filters
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class DisposableAttribute : Attribute
    {
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using RESTfulMapsGateway.Api.Validation;
using RESTfulMapsGateway.Api.Version1.Models.Result.Error;
using RESTfulMapsGateway.Domain.Models.Enums;
using Newtonsoft.Json;

namespace RESTfulMapsGateway.Api.Filters
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public sealed class ValidateModelAttribute : ActionFilterAttribute
    {
        public ValidateModelAttribute()
            : this(arguments =>
                arguments.ContainsValue(null))
        {
        }

        public ValidateModelAttribute(Func<Dictionary<string, object>, bool> validate)
        {
            Validate = validate;
        }

        public Func<Dictionary<string, object>, bool> Validate { get; private set; }


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private static string FetchJsonParameterDetails(HttpActionContext actionContext)
        {
            string parameters = JsonConvert.SerializeObject(actionContext.ActionArguments.FirstOrDefault().Value);

            return parameters;
        }

        private static void ValidateGroupModels(HttpActionContext actionContext)
        {
            if (!actionContext.ActionArguments.Any())
                return;

            var multiModelAttributes = new List<MultiModelPropertyRequiredAttribute>();

            foreach (
                PropertyInfo property in
                    actionContext.ActionArguments.FirstOrDefault()
                        .Value.GetType()
                        .GetProperties()
                        .Where(x => x.CanRead && x.GetCustomAttribute<MultiModelPropertyRequiredAttribute>() != null))
            {
                multiModelAttributes.AddRange(property.GetCustomAttributes<MultiModelPropertyRequiredAttribute>());
            }

            IEnumerable<IGrouping<InputModelType, MultiModelPropertyRequiredAttribute>> groupedModelAttrbutes =
                multiModelAttributes.GroupBy(x => x.ModelType);

            bool validGroup = !groupedModelAttrbutes.Any();

            foreach (var modelGroup in groupedModelAttrbutes.ToList())
            {
                if (modelGroup.All(attribute => actionContext.ModelState.IsValidField(attribute.PropertyName)))
                {
                    validGroup = true;
                }

                foreach (MultiModelPropertyRequiredAttribute attribute in modelGroup)
                {
                    actionContext.ModelState.Remove(attribute.PropertyName);
                }
            }

            if (validGroup) return;

            foreach (
                MultiModelPropertyRequiredAttribute property in
                    groupedModelAttrbutes.SelectMany(modelOption => modelOption))
            {
                actionContext.ModelState.AddModelError(property.PropertyName,
                    "Option " + property.ModelType + ": Property '" + property.PropertyName + "' is required.");
            }
        }


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }

            if (Validate(actionContext.ActionArguments))
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ErrorResponseModel(ErrorCode.InvalidInputModel, Strings.InvalidInputModelMessage));
                return;
            }

            ValidateGroupModels(actionContext);


            //var resource = actionContext.Request.RequestUri.AbsolutePath.Trim('/');
            //var resourceVerb = actionContext.Request.Method.ToString();
            // var logMessage = String.Format("{0} {1}", resourceVerb, resource);

            //var agent = actionContext.Request.Headers.UserAgent.ToString();
            //var platform = UserAgentParser.GetOperatingSystem(agent);

            //var inputparams =FetchJsonParameterDetails(actionContext);

            IEnumerable<string> tokenValue = actionContext.Request.Headers.SingleOrDefault(x => x.Key == "Token").Value;
            if (tokenValue != null)
            {
                //inputparams = "Token: \"" + tokenValue.First() + "\"," + inputparams;
            }
            IEnumerable<string> appkeyValue =
                actionContext.Request.Headers.SingleOrDefault(x => x.Key == "AppKey").Value;
            if (appkeyValue != null)
            {
                //inputparams = "Appkey: \"" + appkeyValue.First() + "\"," + inputparams;
            }


            //Logger.Instance.LogInfo(logMessage, Strings.ApiCallStartMessage, inputparams, platform, agent);

            if (actionContext.ModelState.IsValid == false)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                    new ErrorValidationResponseModel(ErrorCode.InvalidInputModel, Strings.InvalidInputModelMessage,
                        actionContext.ModelState));
            }
        }
    }
}
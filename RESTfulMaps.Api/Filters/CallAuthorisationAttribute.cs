﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace RESTfulMapsGateway.Api.Filters
{
    public sealed class CallAuthorisationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }


            base.OnAuthorization(actionContext);
        }
    }
}
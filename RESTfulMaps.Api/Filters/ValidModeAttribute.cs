﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RESTfulMapsGateway.Api.Filters
{
    [AttributeUsage(AttributeTargets.Property |
                    AttributeTargets.Field, AllowMultiple = false)]
    public sealed class ValidModeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var mode = (string) value;
            bool result = IsValidMode(mode);
            return result;
        }

        private bool IsValidMode(string mode)
        {
            bool isValid = String.IsNullOrEmpty(mode);

            foreach (string a in Enum.GetNames(typeof (TransportMode)))
            {
                if (a == mode)
                {
                    isValid = true;
                }
            }

            return isValid;
        }

        private enum TransportMode
        {
            Driving,
            Walking,
            Rail
        }
    }
}
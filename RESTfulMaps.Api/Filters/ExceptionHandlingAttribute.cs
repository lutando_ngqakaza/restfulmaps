﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Filters;
using RESTfulMapsGateway.Api.Version1.Models.Result.Error;
using lutz.Logging.Loggers;

namespace RESTfulMapsGateway.Api.Filters
{
    public sealed class ExceptionHandlingAttribute : ExceptionFilterAttribute
    {
        private static HttpContent GetHttpContent(ErrorResponseModel errorResponseModel)
        {
            return new ObjectContent<ErrorResponseModel>(errorResponseModel, new JsonMediaTypeFormatter());
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext == null)
            {
                throw new ArgumentNullException("actionExecutedContext");
            }

            var errorModel = new ErrorResponseModel(ErrorCode.InvalidOperation, Strings.InternalServerErrorMessage);

            string resource = actionExecutedContext.Request.RequestUri.AbsolutePath.Trim('/');
            string resourceVerb = actionExecutedContext.Request.Method.ToString();
            string logMessage = String.Format("{0} {1}", resourceVerb, resource);

            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogFatal(logContext,
                String.Format("Resource:{0} ExceptionMessage:{1}  parameters{2}", logMessage,
                    Strings.FatalExceptionMessage, actionExecutedContext), actionExecutedContext.Exception);

            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = GetHttpContent(errorModel),
                ReasonPhrase = "Exception"
            });
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.Globalization;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.TypeConverters
{
    public class CoordinateTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof (string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            //culture = new CultureInfo("en-GB");

            if (value is string)
            {
                CoordinateModel location;
                if (CoordinateModel.TryParse((string) value, out location))
                {
                    return location;
                }
                object result = null;
                try
                {
                    //result = base.ConvertFrom(context, culture, value);
                }
                catch (Exception)
                {
                    throw new ArgumentException("Point", "context");
                    return result;
                }

                return result;
            }
            throw new ArgumentException("Point", "context");
            //return base.ConvertFrom(context, culture, value);
        }
    }
}
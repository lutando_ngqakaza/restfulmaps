﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using RESTfulMapsGateway.Api.DependancyResolution;
using RESTfulMapsGateway.Api.Filters;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Serialization;
using lutz.Logging.Loggers;
using lutz.Logging.WebApi.Filters;

namespace RESTfulMapsGateway.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // IOC container
            var container = new UnityContainer();
            config.DependencyResolver = new UnityResolver(container);

            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");

            logger.LogInfo(logContext, "Registering Components.");

            // IOC resolution
            var resolver = new Resolver();
            resolver.RegisterTypes(container);


            // Web API routes
            config.MapHttpAttributeRoutes();


            logger.LogTrace(logContext, "Configured HTTPRoutes...");

            config.Filters.Add(new CallAuthorisationAttribute());
            config.Filters.Add(new LoggingExecutedAttribute());
            config.Filters.Add(new ExceptionHandlingAttribute());
            config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new LogAttribute("Api"));

            logger.LogTrace(logContext, "Registered Filters...");

            //JSON formatting
            JsonMediaTypeFormatter jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Culture = new CultureInfo("en-ZA");
        }
    }
}
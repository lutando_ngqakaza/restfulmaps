﻿using System;
using System.Web.Http;
using Swashbuckle.Application;

namespace RESTfulMapsGateway.Api.App_Start
{
    public static class SwaggerConfig
    {
        internal static void Register(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "RESTfulMaps Gateway");
                    c.IncludeXmlComments(GetXmlCommentsPath());
                })
                .EnableSwaggerUi();
        }

        private static string GetXmlCommentsPath()
        {
            return String.Format(@"{0}bin\RESTfulMapsGateway.Api.xml", AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using Microsoft.Azure;
using Microsoft.Web.Administration;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using lutz.Logging.Loggers;

namespace RESTfulMapsGateway.Api.App_Start
{
    public class WebRole : RoleEntryPoint
    {
        private static readonly string StorageAccount = CloudConfigurationManager.GetSetting("Storage.Account");

        private static readonly string StorageKey = CloudConfigurationManager.GetSetting("Storage.Key");
        private string noReplyAddress;

        private string supportAddress;

        public string NoReplyAddress
        {
            get
            {
                if (noReplyAddress == null)
                {
                    noReplyAddress = CloudConfigurationManager.GetSetting("Email.NoReply");
                }

                return noReplyAddress;
            }
        }

        public string SupportAddress
        {
            get
            {
                if (supportAddress == null)
                {
                    supportAddress = CloudConfigurationManager.GetSetting("Email.lutzSupport");
                }

                return supportAddress;
            }
        }

        [LoggingMethod]
        private static void DownloadBlobToFile(string accountName, string accountKey)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");


            if (!File.Exists("C:\\DDS.zip"))
            {
                logger.LogInfo(logContext, "Downloading file from Azure Blob Storage");

                var credentials = new StorageCredentials(accountName, accountKey);
                string blobFileName = CloudConfigurationManager.GetSetting("Dds.StorageFileName");
                var account = new CloudStorageAccount(credentials, true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer scriptContainer = client.GetContainerReference("dds");

                CloudBlockBlob blob = scriptContainer.GetBlockBlobReference(blobFileName);


                using (Stream outputfile = new FileStream("C:\\DDS.zip", FileMode.Create))
                {
                    blob.DownloadToStream(outputfile);
                }
                logger.LogTrace(logContext, "Download of Blob complete...");
            }

            string zipPath = "C:\\DDS.zip";
            string extractPath = "C:\\";
            if (!File.Exists("C:\\DDS"))
            {
                try
                {
                    logger.LogTrace(logContext, "Unzipping Blob file...");
                    ZipFile.ExtractToDirectory(zipPath, extractPath);
                }
                catch (Exception ex)
                {
                    logger.LogFatal(logContext, "Could not unzip file", ex);
                }
            }

            logger.LogTrace(logContext, "Unzipping of Blob complete...");
        }

        [LoggingMethod]
        private static void StartApacheWebService()
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, "Starting (DDS) Apache Web Service.");


            string fileName = "C:\\DDS1\\Web_Services\\startServer-WSOnly.bat";
            Trace.TraceInformation("FileName {0} !!", fileName);
            if (File.Exists(fileName))
            {
                logger.LogTrace(logContext, "Installing using batch script...");
                var processInfo = new ProcessStartInfo("cmd.exe", "/c START " + fileName);

                processInfo.CreateNoWindow = false;

                processInfo.UseShellExecute = false;

                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                Process process = Process.Start(processInfo);

                //process.Start();
                //test
                process.WaitForExit();
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        [LoggingMethod]
        private static bool StartService(ServiceController controller)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            try
            {
                controller.Start();

                logger.LogInfo(logContext, String.Format("Starting Service: {0}", controller.ServiceName));
                return true;
            }
            catch (Exception ex)
            {
                logger.LogFatal(logContext, String.Format("DDS Service {0} failed to start.", controller.ServiceName),
                    ex);
                return false;
            }
        }

        [LoggingMethod]
        private static bool StartService(string serviceName)
        {
            ServiceController controller = null;
            try
            {
                controller = new ServiceController(serviceName);
                controller.Start();
                return true;
            }
            catch (Exception ex)
            {
                MethodBase info = MethodBase.GetCurrentMethod();
                string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
                ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
                logger.LogError(logContext, "Service failed to start", ex);
                return false;
            }
        }

        private static bool DoesServiceExist(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices(Environment.MachineName);
            ServiceController service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            return service != null;
        }

        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults",
            MessageId = "System.ServiceProcess.ServiceController")]
        [LoggingMethod]
        private static bool CheckService(string serviceName)
        {
            if (!DoesServiceExist(serviceName))
            {
                InstallDdsService(serviceName);
            }

            var controller = new ServiceController(serviceName);
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            //if controller.
            switch (controller.Status)
            {
                case ServiceControllerStatus.Running:
                    logger.LogInfo(logContext, String.Format("Service {0} is in a running state", serviceName));
                    return true;
                case ServiceControllerStatus.Stopped:
                    logger.LogInfo(logContext, String.Format("Service {0} is in a stopped state", serviceName));
                    return StartService(controller);
                case ServiceControllerStatus.Paused:
                    logger.LogInfo(logContext, String.Format("Service {0} is in a stopped state", serviceName));
                    return StartService(controller);
                case ServiceControllerStatus.StopPending:
                    logger.LogInfo(logContext, String.Format("Service {0} is in a stop-pending state", serviceName));
                    return StartService(controller);
                case ServiceControllerStatus.StartPending:
                    logger.LogInfo(logContext, String.Format("Service {0} is in a start-pending state", serviceName));
                    return StartService(controller);
                default:
                    return false;
            }
            //return true;
        }

        [LoggingMethod]
        private static void InstallDdsService(string serviceName)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("Attempting to install service {0}.", serviceName));

            string fileName = String.Format("C:\\{0}.bat", serviceName);

            Trace.TraceInformation("FileName {0} !!", serviceName);
            if (File.Exists(fileName))
            {
                var processInfo = new ProcessStartInfo("cmd.exe", "/c START " + fileName);

                processInfo.CreateNoWindow = true;

                processInfo.UseShellExecute = false;

                processInfo.RedirectStandardError = true;
                processInfo.RedirectStandardOutput = true;

                Process process = Process.Start(processInfo);

                //process.Start();
                //test
                process.WaitForExit();
            }
        }


        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
        public override bool OnStart()
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, "Web Role Starting Up.");
            logger.LogTrace(logContext, "Starting Pre-Role Configuration...");

            if (!RoleEnvironment.IsEmulated)
            {
                string[] services = CloudConfigurationManager.GetSetting("Dds.ServiceNames").Split(',');

                DownloadBlobToFile(StorageAccount, StorageKey);

                foreach (string service in services)
                {
                    if (CheckService(service))
                    {
                        StartService(service);
                    }
                }

                DdsConfig.ConfigureDdsServiceConfiguration();
                DdsConfig.ConfigureDdsWebConfiguration();

                StartApacheWebService();
            }
            else
            {
                DdsConfig.ConfigureDdsServiceConfiguration();
                DdsConfig.ConfigureDdsWebConfiguration();
            }

            logger.LogTrace(logContext, "Running WebRole.OnStart()");


            if (!RoleEnvironment.IsEmulated)
            {
                logger.LogTrace(logContext, "Configuring Role Warm-up...");

                try
                {
                    using (var serverManager = new ServerManager())
                    {
                        serverManager.ApplicationPoolDefaults.ProcessModel.IdleTimeout = TimeSpan.Zero;

                        foreach (Application application in serverManager.Sites.SelectMany(x => x.Applications))
                        {
                            application["preloadEnabled"] = true;
                        }

                        foreach (ApplicationPool applicationPool in serverManager.ApplicationPools)
                        {
                            applicationPool.AutoStart = true;
                            applicationPool["startMode"] = "AlwaysRunning";
                            applicationPool.ProcessModel.IdleTimeout = TimeSpan.Zero;
                            applicationPool.Recycling.PeriodicRestart.Time = TimeSpan.Zero;
                        }

                        serverManager.CommitChanges();
                    }

                    using (var serverManager = new ServerManager())
                    {
                        Configuration config = serverManager.GetApplicationHostConfiguration();
                        ConfigurationSection httpCompressionSection =
                            config.GetSection("system.webServer/httpCompression");
                        ConfigurationElementCollection staticTypesCollection =
                            httpCompressionSection.GetCollection("dynamicTypes");

                        ConfigurationElement addElementJson = staticTypesCollection.CreateElement("add");
                        addElementJson["mimeType"] = @"application/json";
                        addElementJson["enabled"] = true;
                        staticTypesCollection.Add(addElementJson);

                        ConfigurationElement addElementXml = staticTypesCollection.CreateElement("add");
                        addElementXml["mimeType"] = @"application/xml";
                        addElementXml["enabled"] = true;
                        staticTypesCollection.Add(addElementXml);

                        serverManager.CommitChanges();
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(logContext, "Role Warm-up configuration failed.", ex);
                }

                logger.LogInfo(logContext,
                    String.Format("Warm-up configuration successful on role {0}.",
                        RoleEnvironment.CurrentRoleInstance.Role.Name));
            }

            return base.OnStart();
        }

        [LoggingMethod]
        public override void OnStop()
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, "Running WebRole.OnStop()");


            base.OnStop();
        }
    }
}
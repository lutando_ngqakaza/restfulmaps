﻿using System;
using System.Reflection;
using System.Xml;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using Microsoft.Azure;
using lutz.Logging.Loggers;

namespace RESTfulMapsGateway.Api.App_Start
{
    public static class DdsConfig
    {
        [LoggingMethod]
        public static void ConfigureDdsWebConfiguration()
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("Configuring DDS Web Service."));

            var document = new XmlDocument();

            string filePath = CloudConfigurationManager.GetSetting("Dds.FilePath") +
                              "\\server\\Tomcat 6.0.18\\webapps\\openls\\WEB-INF\\classes\\service-configuration.xml";

            document.Load(filePath);


            XmlElement root = document.DocumentElement;

            XmlNode node = root.GetElementsByTagName("client-clone")[0];

            node.Attributes["password"].Value = CloudConfigurationManager.GetSetting("Dds.WebService.Password");
            node.Attributes["name"].Value = CloudConfigurationManager.GetSetting("Dds.WebService.Username");

            document.Save(filePath);

            logger.LogTrace(logContext, String.Format("DDS Web Configuration Saved..."));
        }

        [LoggingMethod]
        public static void ConfigureDdsServiceConfiguration()
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("Configuring DDS Service."));


            var document = new XmlDocument();

            string filePath = CloudConfigurationManager.GetSetting("Dds.FilePath") +
                              "\\server\\Tomcat 6.0.18\\webapps\\openls\\WEB-INF\\web.xml";

            document.Load(filePath);

            XmlElement root = document.DocumentElement;

            XmlNodeList node = root.GetElementsByTagName("context-param");

            foreach (XmlNode child in node)
            {
                XmlNode para = child.SelectNodes("param-name")[0];
                if (para != null)
                {
                    if (para.InnerText == "external-hostname")
                    {
                        para.NextSibling.InnerText =
                            CloudConfigurationManager.GetSetting("Dds.WebService.ExternalHostname");
                    }
                }
            }

            document.Save(filePath);
            logger.LogTrace(logContext, String.Format("DDS Configuration saved..."));
        }
    }
}
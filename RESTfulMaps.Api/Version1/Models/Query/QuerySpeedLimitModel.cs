﻿using System;
using RESTfulMapsGateway.Api.Validation;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version1.Models.Query
{
    public class QuerySpeedLimitModel
    {
        [PropertyRequired]
        public CoordinateModel Location { get; set; }

        public override string ToString()
        {
            return String.Format("SpeedLocation={0}", Location);
        }
    }
}
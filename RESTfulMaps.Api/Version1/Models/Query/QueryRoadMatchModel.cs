﻿using System;
using RESTfulMapsGateway.Api.Validation;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version1.Models.Query
{
    public class QueryRoadMatchModel
    {
        //[TypeConverter(typeof(CoordinateTypeConverter))]
        [PropertyRequired]
        public CoordinateModel Location { get; set; }

        public Double Bearing { get; set; }

        public override string ToString()
        {
            return String.Format("Location={0} Bearing={1}", Location, Bearing);
        }
    }
}
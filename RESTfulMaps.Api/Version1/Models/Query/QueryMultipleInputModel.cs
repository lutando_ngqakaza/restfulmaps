﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RESTfulMapsGateway.Api.Validation;
using RESTfulMapsGateway.Domain.Models.Enums;

namespace RESTfulMapsGateway.Api.Version1.Models.Query
{
    public class QueryMultipleInputModel
    {
        public InputModelType ValidModel
        {
            get
            {
                var multiModelAttributes = new List<MultiModelPropertyRequiredAttribute>();

                foreach (
                    PropertyInfo property in
                        GetType()
                            .GetProperties()
                            .Where(x => x.CanRead && x.GetCustomAttribute<MultiModelPropertyRequiredAttribute>() != null)
                    )
                {
                    IEnumerable<MultiModelPropertyRequiredAttribute> validatedAttributes = property
                        .GetCustomAttributes<MultiModelPropertyRequiredAttribute>().Select(
                            x =>
                            {
                                x.IsValid(property.GetValue(this, null));
                                return x;
                            });

                    multiModelAttributes.AddRange(validatedAttributes);
                }

                IEnumerable<IGrouping<InputModelType, MultiModelPropertyRequiredAttribute>> groupedModelAttrbutes =
                    multiModelAttributes.GroupBy(x => x.ModelType);

                IGrouping<InputModelType, MultiModelPropertyRequiredAttribute> validModel =
                    groupedModelAttrbutes.FirstOrDefault(x => x.All(y => y.Valid));

                if (validModel != null)
                    return validModel.Key;
                throw new InvalidOperationException("A valid model should exist, but didn't");
            }
        }
    }
}
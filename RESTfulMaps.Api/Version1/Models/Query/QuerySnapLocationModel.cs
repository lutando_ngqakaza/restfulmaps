﻿using System;
using RESTfulMapsGateway.Api.Validation;

namespace RESTfulMapsGateway.Api.Version1.Models.Query
{
    public class QuerySnapLocationModel
    {
        /// <summary>
        ///     Optional latitude co-ordinate of the user.
        /// </summary>
        [PropertyRequired]
        public Double Latitude { get; set; }

        /// <summary>
        ///     Optional longitude co-ordinate of the user.
        /// </summary>
        [PropertyRequired]
        public Double Longitude { get; set; }

        public override string ToString()
        {
            return String.Format("SnapLocation={0},{1}", Latitude, Longitude);
        }
    }
}
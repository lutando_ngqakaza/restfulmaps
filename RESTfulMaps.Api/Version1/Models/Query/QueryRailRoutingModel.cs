﻿using System;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version1.Models.Query
{
    public class QueryRailRoutingModel
    {
        //[PropertyRequired]
        //[TypeConverter(typeof(CoordinateTypeConverter))]
        public CoordinateModel StartingLocation { get; set; }

        //[TypeConverter(typeof(CoordinateTypeConverter))]
        //[PropertyRequired]
        public CoordinateModel DestinationLocation { get; set; }

        //public Coordinate StartingLocation { get; set; }

        //public Coordinate DestinationLocation { get; set; }

        public Double StartingLatitude { get; set; }

        public Double StartingLongitude { get; set; }

        public Double DestinationLatitude { get; set; }

        public Double DestinationLongitude { get; set; }

        public override string ToString()
        {
            return String.Format("StartingLocation={0} DestinationLocation={1}", StartingLocation, DestinationLocation);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using RESTfulMapsGateway.Api.Version1.Models.Data;
using RESTfulMapsGateway.Api.Version1.Models.Result;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Models
{
    public static class ResponseBuilder
    {
        public static GetRoadMatchResultModel BuildRoadMatchModel(RoadDetails details)
        {
            return new GetRoadMatchResultModel(details);
        }

        public static GetSnappedLocationResultModel BuildSnappedLocationModel(Coordinate location)
        {
            return new GetSnappedLocationResultModel(location);
        }


        public static GetRailRoutingResultModel BuildRailRoutingResult(IEnumerable<Coordinate> route)
        {
            return new GetRailRoutingResultModel(
                route.Select(x => new CoordinateModel(x.Latitude, x.Longitude)));
        }


        public static GetSpeedLimitResultModel BuildSpeedLimitResult(int speedLimit)
        {
            return new GetSpeedLimitResultModel(speedLimit);
        }
    }
}
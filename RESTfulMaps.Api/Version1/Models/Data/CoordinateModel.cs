﻿using System;
using System.ComponentModel;
using System.Globalization;
using RESTfulMapsGateway.Api.TypeConverters;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Models.Data
{
    [TypeConverter(typeof (CoordinateTypeConverter))]
    public class CoordinateModel
    {
        private double latitude;
        private double longitude;

        public CoordinateModel()
        {
        }

        public CoordinateModel(Coordinate location)
        {
            Longitude = location.Longitude;
            Latitude = location.Latitude;
        }

        public CoordinateModel(double latitude, double longitude)
        {
            Longitude = longitude;
            Latitude = latitude;
        }

        /// <summary>
        ///     Decimal latitude value between -90 and 90.
        /// </summary>
        public double Latitude
        {
            get { return latitude; }
            private set
            {
                if (value < -90 || value > 90)
                {
                    throw new ArgumentOutOfRangeException("value",
                        "Latitude must be between -90 and 90 degrees inclusive");
                }

                latitude = value;
            }
        }

        /// <summary>
        ///     Decimal longitude value between -180 and 180.
        /// </summary>
        public double Longitude
        {
            get { return longitude; }
            private set
            {
                if (value < -180 || value > 180)
                {
                    throw new ArgumentOutOfRangeException("value",
                        "Longitude must be between -180 and 180 degrees inclusive");
                }

                longitude = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}", Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat),
                Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat));
        }

        public static bool IsLatitude(double latitude)
        {
            if (latitude < -90 || latitude > 90)
            {
                //throw appropriate exception
                return false;
            }
            return true;
        }

        public static bool IsLongitude(double latitude)
        {
            if (latitude < -180 || latitude > 180)
            {
                //throw appropriate exception
                return false;
            }
            return true;
        }

        public static bool TryParse(String input, out CoordinateModel result)
        {
            result = null;

            string[] parts = input.Split(',');
            if (parts.Length != 2)
            {
                //throw appropriate exception 
                return false;
            }

            double longitude, latitude;
            //if(decimal.TryParse(parts[0],)
            bool latParsed = Double.TryParse(parts[0], NumberStyles.Any, CultureInfo.InvariantCulture, out latitude);
            bool longParsed = Double.TryParse(parts[1], NumberStyles.Any, CultureInfo.InvariantCulture, out longitude);

            //if(Decimal.TryParse(parts[0], NumberStyles.Any, CultureInfo.InvariantCulture))

            if (latParsed && longParsed)
            {
                result = new CoordinateModel(latitude, longitude);
                return true;
            }

            /* if (Decimal.TryParse(parts[0], NumberStyles.Any, culture,  out latitude) && Decimal.TryParse(parts[1], NumberStyles.Any, culture, out longitude))
            {
                result = new CoordinateModel(latitude, longitude);
                return true;
            }*/

            //throw appropriate exception
            return false;
        }

        public static CoordinateModel operator +(CoordinateModel c1, CoordinateModel c2)
        {
            return new CoordinateModel(c1.latitude + c2.latitude, c1.longitude + c2.longitude);
        }

        public static CoordinateModel Add(CoordinateModel c1, CoordinateModel c2)
        {
            return new CoordinateModel(c1.latitude + c2.latitude, c1.longitude + c2.longitude);
        }


        public static CoordinateModel Divide(CoordinateModel c, int d)
        {
            return new CoordinateModel(c.latitude/d, c.longitude/d);
        }

        public static CoordinateModel operator /(CoordinateModel c, int d)
        {
            return new CoordinateModel(c.latitude/d, c.longitude/d);
        }

        public static bool operator ==(CoordinateModel c1, CoordinateModel c2)
        {
            if (c1.latitude == c2.latitude && c1.longitude == c2.longitude)
            {
                return true;
            }

            return false;
        }

        public static bool operator !=(CoordinateModel c1, CoordinateModel c2)
        {
            if (c1.latitude != c2.latitude && c1.longitude != c2.longitude)
            {
                return true;
            }

            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is CoordinateModel))
                return false;
            return this == (CoordinateModel) obj;
        }

        public override int GetHashCode()
        {
            return latitude.GetHashCode() + longitude.GetHashCode();
        }
    }
}
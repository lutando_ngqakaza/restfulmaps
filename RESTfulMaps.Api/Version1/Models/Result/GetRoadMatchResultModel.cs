﻿using System;
using RESTfulMapsGateway.Api.Version1.Models.Data;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Models.Result
{
    public class GetRoadMatchResultModel
    {
        public GetRoadMatchResultModel(CoordinateModel location)
        {
            Location = location;
        }

        public GetRoadMatchResultModel(Coordinate location)
        {
            Location = new CoordinateModel(location.Latitude, location.Longitude);
        }

        public GetRoadMatchResultModel(RoadDetails details)
        {
            Location = new CoordinateModel(details.Location.Latitude, details.Location.Longitude);
            SpeedLimit = details.SpeedLimit;
            StreetName = details.StreetName;
            City = details.City;
            Suburb = details.Suburb;
            Province = details.Province;
        }

        public CoordinateModel Location { get; private set; }

        public String Suburb { get; set; }

        public String City { get; set; }

        public String Province { get; set; }

        public Decimal SpeedLimit { get; set; }

        public String StreetName { get; set; }
    }
}
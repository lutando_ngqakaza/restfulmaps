﻿using System;

namespace RESTfulMapsGateway.Api.Version1.Models.Result
{
    public class GetSpeedLimitResultModel
    {
        public GetSpeedLimitResultModel(int speedLimit)
        {
            SpeedLimit = speedLimit;
        }

        public Int32 SpeedLimit { get; set; }
    }
}
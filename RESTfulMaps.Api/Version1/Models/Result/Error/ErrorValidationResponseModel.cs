﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace RESTfulMapsGateway.Api.Version1.Models.Result.Error
{
    public class ErrorValidationResponseModel
    {
        private readonly ModelStateDictionary modelState;

        public ErrorValidationResponseModel(ErrorCode errorCode, string message, ModelStateDictionary modelState)
        {
            ErrorCode = (int) errorCode;
            Message = message;
            this.modelState = modelState;
        }

        public int ErrorCode { get; set; }

        public string Message { get; set; }

        public Dictionary<string, List<string>> ModelState
        {
            get
            {
                var modelStates = new Dictionary<string, List<string>>();

                foreach (var model in modelState)
                {
                    if (modelState.IsValidField(model.Key))
                        continue;

                    List<string> errors = model.Value.Errors.Select(error => error.ErrorMessage).ToList();

                    modelStates.Add(model.Key, errors);
                }

                return modelStates;
            }
        }
    }
}
﻿namespace RESTfulMapsGateway.Api.Version1.Models.Result.Error
{
    public class ErrorResponseModel
    {
        public ErrorResponseModel(ErrorCode errorCode, string message)
        {
            ErrorCode = (int) errorCode;
            Message = message;
        }

        public int ErrorCode { get; set; }

        public string Message { get; set; }
    }
}
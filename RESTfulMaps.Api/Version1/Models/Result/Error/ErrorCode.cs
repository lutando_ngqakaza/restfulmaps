﻿namespace RESTfulMapsGateway.Api.Version1.Models.Result.Error
{
    public enum ErrorCode
    {
        None = 0,
        InvalidInputModel = 1001,
        InvalidOperation = 1002,
        InvalidAppKey = 1010,
        OutOfDateAppKey = 1011,
        InactiveAppKey = 1012,
        IncorrectDomainAppKey = 1013,
        InvalidToken = 1020,
        InactiveToken = 1021,
        UserNotActive = 1022,
        UserRateLimitExceeded = 1023,
        EmailDoesntExist = 1030,
        EmailIsRegistered = 1031,
        EmailAlreadyAuthenticated = 1032,
        EmailNotChanged = 1033,
        PinExpiredOrNotFound = 1040,
        SearchOrCategoryRequired = 1052,
        NoRouteFound = 1060
    }
}
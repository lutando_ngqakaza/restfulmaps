﻿using RESTfulMapsGateway.Api.Version1.Models.Data;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Models.Result
{
    public class GetSnappedLocationResultModel
    {
        public GetSnappedLocationResultModel(CoordinateModel location)
        {
            Location = location;
        }

        public GetSnappedLocationResultModel(Coordinate location)
        {
            Location = new CoordinateModel(location.Latitude, location.Longitude);
        }

        public CoordinateModel Location { get; private set; }
    }
}
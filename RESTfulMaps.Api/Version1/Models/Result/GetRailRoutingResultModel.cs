﻿using System.Collections.Generic;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version1.Models.Result
{
    public class GetRailRoutingResultModel
    {
        public GetRailRoutingResultModel()
        {
        }

        public GetRailRoutingResultModel(IEnumerable<CoordinateModel> route)
        {
            Route = route;
        }

        public IEnumerable<CoordinateModel> Route { get; set; }
    }
}
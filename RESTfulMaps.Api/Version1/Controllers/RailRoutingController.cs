﻿//using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;

using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RESTfulMapsGateway.Api.Version1.Models;
using RESTfulMapsGateway.Api.Version1.Models.Query;
using RESTfulMapsGateway.Api.Version1.Models.Result;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Controllers
{
    [RoutePrefix("api/railrouting")]
    public class RailRoutingController : BaseApiController
    {
        public RailRoutingController(IRailRoutingService railRoutingService)
        {
            RailRoutingService = railRoutingService;
        }

        protected IRailRoutingService RailRoutingService { get; set; }

        /// <summary>
        ///     Get Rail Routes.
        /// </summary>
        /// <param name="model">The identifier of the company.</param>
        /// <returns>The rail route as a set of points</returns>
        [Route]
        [LoggingMethod]
        public HttpResponseMessage Get([FromUri] QueryRailRoutingModel model)
        {
            /*var info = MethodBase.GetCurrentMethod();
            var logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            var logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}",model));*/

            var starting = new Coordinate(model.StartingLocation.Latitude, model.StartingLocation.Longitude);
            var destination = new Coordinate(model.DestinationLocation.Latitude, model.DestinationLocation.Longitude);

            IEnumerable<Coordinate> route = RailRoutingService.GetRailRoute(starting, destination);

            GetRailRoutingResultModel response = ResponseBuilder.BuildRailRoutingResult(route);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
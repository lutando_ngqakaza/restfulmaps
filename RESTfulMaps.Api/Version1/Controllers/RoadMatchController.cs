﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using RESTfulMapsGateway.Api.Version1.Models;
using RESTfulMapsGateway.Api.Version1.Models.Query;
using RESTfulMapsGateway.Api.Version1.Models.Result;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using lutz.Logging.Loggers;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Controllers
{
    [RoutePrefix("api/roadmatch")]
    public class RoadMatchController : BaseApiController
    {
        public RoadMatchController(IRoadMatchService roadMatchService)
        {
            RoadMatchService = roadMatchService;
        }

        protected IRoadMatchService RoadMatchService { get; set; }

        /// <summary>
        ///     Match a point onto a linear road feature.
        /// </summary>
        /// <param name="model">The point that needs to be matched onto the linear feature</param>
        /// <returns>The corrected point on the linear feature</returns>
        [Route]
        [LoggingMethod]
        public HttpResponseMessage Get([FromUri] QueryRoadMatchModel model)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}", model));

            var queryLocation = new Coordinate(model.Location.Latitude, model.Location.Longitude);

            RoadDetails roadMatchResponse = null;

            if (model.Bearing > 0)
            {
                roadMatchResponse = RoadMatchService.GetRoadMatch(queryLocation, model.Bearing);
            }
            else
            {
                roadMatchResponse = RoadMatchService.GetRoadMatch(queryLocation);
            }


            GetRoadMatchResultModel response = ResponseBuilder.BuildRoadMatchModel(roadMatchResponse);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
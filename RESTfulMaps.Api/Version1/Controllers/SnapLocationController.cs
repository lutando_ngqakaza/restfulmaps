﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using RESTfulMapsGateway.Api.Version1.Models;
using RESTfulMapsGateway.Api.Version1.Models.Query;
using RESTfulMapsGateway.Api.Version1.Models.Result;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using lutz.Logging.Loggers;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Controllers
{
    [RoutePrefix("api/snaplocation")]
    public class SnapLocationController : BaseApiController
    {
        public SnapLocationController(ISnapLocationService snapLocationService)
        {
            SnapLocationService = snapLocationService;
        }

        private ISnapLocationService SnapLocationService { get; set; }

        /// <summary>
        ///     Match a point onto a map feature.
        /// </summary>
        /// <param name="model">The point that needs to be matched onto a map feature</param>
        /// <returns>The corrected point on the RESTfulMaps feature</returns>
        [Route]
        [LoggingMethod]
        public HttpResponseMessage Get([FromUri] QuerySnapLocationModel model)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}", model));

            Coordinate snapLocation =
                SnapLocationService.GetSnappedLocation(new Coordinate(model.Latitude, model.Longitude));

            GetSnappedLocationResultModel response = ResponseBuilder.BuildSnappedLocationModel(snapLocation);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using RESTfulMapsGateway.Api.Version1.Models;
using RESTfulMapsGateway.Api.Version1.Models.Query;
using RESTfulMapsGateway.Api.Version1.Models.Result;
using RESTfulMapsGateway.Domain.Interfaces.Services;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using lutz.Logging.Loggers;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version1.Controllers
{
    [RoutePrefix("api/speedlimit")]
    public class SpeedLimitController : BaseApiController
    {
        public SpeedLimitController(IRESTfulMapsService RESTfulMapsService)
        {
            RESTfulMapsService = RESTfulMapsService;
        }

        protected IRESTfulMapsService RESTfulMapsService { get; set; }

        /// <summary>
        ///     Get the speedlimit of a road
        /// </summary>
        /// <param name="model">The point at which the speedlimit needs to be retrieved </param>
        /// <returns>The corrected point on the linear feature</returns>
        [Route]
        [LoggingMethod]
        public HttpResponseMessage Get([FromUri] QuerySpeedLimitModel model)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}", model));


            var queryLocation = new Coordinate(model.Location.Latitude, model.Location.Longitude);

            int speedLimit = RESTfulMapsService.GetSpeedLimit(queryLocation);

            GetSpeedLimitResultModel response = ResponseBuilder.BuildSpeedLimitResult(speedLimit);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
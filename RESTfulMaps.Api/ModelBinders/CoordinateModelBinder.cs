﻿using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.ModelBinders
{
    public class CoordinateModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof (Coordinate))
            {
                return false;
            }

            ValueProviderResult val = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (val == null)
            {
                return false;
            }

            var key = val.RawValue as string;

            if (key == null)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, "Wrong value type");
                return false;
            }

            Coordinate result;
            /*if(Coordinate.TryParse(key, out result))
            {
                bindingContext.Model = result;
                return true;
            }*/
            if (Coordinate.IsCoordinate(key))
            {
                result = new Coordinate(key);
                bindingContext.Model = result;
            }

            bindingContext.ModelState.AddModelError(bindingContext.ModelName, "Cannot convert value to location");
            return false;
        }
    }
}
﻿using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries;
using Microsoft.Practices.Unity;

namespace RESTfulMapsGateway.Api.DependancyResolution
{
    public class Resolver
    {
        public IUnityContainer RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IRoadMatchService, RoadMatchService>();
            container.RegisterType<IReverseGeocodeService, ReverseGeocodeService>();
            container.RegisterType<IPathingService, PathingService>();
            container.RegisterType<IRailRoutingService, RailRoutingService>();
            container.RegisterType<ISnapLocationService, SnapLocationService>();
            container.RegisterType<IRoadRoutingService, RoadRoutingService>();

            return container;
        }
    }
}
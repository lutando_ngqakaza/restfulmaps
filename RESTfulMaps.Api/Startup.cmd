﻿Echo Installing Application Initialisation

IF "%IsComputeEmulatorRunning%" == "false" (
    PKGMGR.EXE /iu:IIS-ApplicationInit
) ELSE (
    Echo Compute emulator running. Application Initialisation installation ignored.
)
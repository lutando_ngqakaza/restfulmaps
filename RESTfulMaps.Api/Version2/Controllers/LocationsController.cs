﻿using System.Web.Http;
using System.Web.Http.Description;
using RESTfulMapsGateway.Api.Version2.Models.InputModels;
using RESTfulMapsGateway.Api.Version2.Models.ResponseModels;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version2.Controllers
{
    [RoutePrefix("api/v2/locations")]
    public class LocationsController : ApiController
    {
        public LocationsController(IReverseGeocodeService reverseGeocodeService)
        {
            ReverseGeocodeService = reverseGeocodeService;
        }

        private IReverseGeocodeService ReverseGeocodeService { get; set; }

        /// <summary>
        ///     Reverse geocode by a location.
        /// </summary>
        /// <param name="input">The point at which the query needs to be made</param>
        /// <returns>The features describing the request in the form of an address</returns>
        [Route]
        [ResponseType(typeof (LocationsResponseModel))]
        public IHttpActionResult Get([FromUri] LocationsInputModel input)
        {
            var queryLocation = new Coordinate(input.Point.Latitude, input.Point.Longitude);

            RoadDetails result = ReverseGeocodeService.GetReverseGeocode(queryLocation);

            string address = result.Address;

            var locationResult = new LocationsResponseModel(result.Location.ToString(), address);

            return Ok(locationResult);
        }
    }
}
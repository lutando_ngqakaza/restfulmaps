﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using RESTfulMapsGateway.Api.Version2.Models.InputModels;
using RESTfulMapsGateway.Api.Version2.Models.ResponseModels;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version2.Controllers
{
    [RoutePrefix("api/v2/roads")]
    public class RoadsController : ApiController
    {
        public RoadsController(IRoadMatchService roadMatchService)
        {
            RoadMatchService = roadMatchService;
        }

        protected IRoadMatchService RoadMatchService { get; set; }

        /// <summary>
        ///     Get the features of a road.
        /// </summary>
        /// <param name="input">The road query point</param>
        /// <returns>The roads features</returns>
        [Route]
        [ResponseType(typeof (RoadsResponseModel))]
        public IHttpActionResult Get([FromUri] RoadsInputModel input)
        {
            var queryLocation = new Coordinate(input.Point.Latitude, input.Point.Longitude);

            string address = String.Empty;
            double speedLimit = 0.0;
            string point = String.Empty;

            if (input.Bearing == 0)
            {
                RoadDetails result = RoadMatchService.GetRoadMatch(queryLocation);
                address = result.Address;
                point = result.Location.ToString();
                speedLimit = Double.Parse(result.SpeedLimit.ToString());
            }
            else
            {
                RoadDetails result = RoadMatchService.GetRoadMatch(queryLocation, input.Bearing);
                address = result.Address;
                point = result.Location.ToString();
                speedLimit = Double.Parse(result.SpeedLimit.ToString());
            }

            var road = new RoadsResponseModel(point, speedLimit, address);

            return Ok(road);
        }
    }
}
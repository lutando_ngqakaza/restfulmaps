﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using RESTfulMapsGateway.Api.Version1.Models.Data;
using RESTfulMapsGateway.Api.Version2.Models.InputModels;
using RESTfulMapsGateway.Api.Version2.Models.ResponseModels;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.Enums;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Api.Version2.Controllers
{
    [RoutePrefix("api/v2/paths")]
    public class PathsController : ApiController
    {
        public PathsController(IPathingService pathingService)
        {
            PathingService = pathingService;
        }

        private IPathingService PathingService { get; set; }

        [Route]
        [ResponseType(typeof (PathsResponseModel))]
        public IHttpActionResult Get([FromUri] PathsInputModel input)
        {
            var points = new List<Coordinate>();

            foreach (CoordinateModel point in input.Points)
            {
                points.Add(new Coordinate(point.Latitude, point.Longitude));
            }

            var mode = TransitMode.Driving;

            if (input.Mode == null || input.Mode == "driving")
            {
                mode = TransitMode.Driving;
            }
            else if (input.Mode == "rail")
            {
                mode = TransitMode.Rail;
            }
            else if (input.Mode == "walking")
            {
                mode = TransitMode.Walking;
            }

            var result = new PathsResponseModel(PathingService.GetRoute(points, mode));


            return Ok(result);
        }
    }
}
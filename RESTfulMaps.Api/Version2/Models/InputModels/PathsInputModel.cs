﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Api.Filters;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version2.Models.InputModels
{
    /// <summary>
    ///     The Paths input model.
    /// </summary>
    public class PathsInputModel
    {
        /// <summary>
        ///     The sequence of requery points that the path query must follow.
        /// </summary>
        public IEnumerable<CoordinateModel> Points { get; set; }

        /// <summary>
        ///     The transit mode that the query must use.
        /// </summary>
        [ValidMode]
        public String Mode { get; set; }
    }
}
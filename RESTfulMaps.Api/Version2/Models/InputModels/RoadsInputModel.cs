﻿using System;
using System.ComponentModel.DataAnnotations;
using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version2.Models.InputModels
{
    /// <summary>
    ///     The Roads input model.
    /// </summary>
    public class RoadsInputModel
    {
        /// <summary>
        ///     The point of the road that needs to be queried.
        /// </summary>
        [Required]
        public CoordinateModel Point { get; set; }

        /// <summary>
        ///     The bearing of the query point.
        /// </summary>
        public Int32 Bearing { get; set; }
    }
}
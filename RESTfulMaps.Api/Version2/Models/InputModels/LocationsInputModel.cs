﻿using RESTfulMapsGateway.Api.Version1.Models.Data;

namespace RESTfulMapsGateway.Api.Version2.Models.InputModels
{
    /// <summary>
    ///     The Locations input model.
    /// </summary>
    public class LocationsInputModel
    {
        /// <summary>
        ///     The point of the location that must be queried.
        /// </summary>
        public CoordinateModel Point { get; set; }
    }
}
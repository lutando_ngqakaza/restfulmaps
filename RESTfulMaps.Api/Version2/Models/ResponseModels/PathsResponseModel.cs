﻿using System;
using System.Collections.Generic;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;

namespace RESTfulMapsGateway.Api.Version2.Models.ResponseModels
{
    /// <summary>
    ///     The Paths response model.
    /// </summary>
    public class PathsResponseModel
    {
        public PathsResponseModel(RouteDetails details)
        {
            Duration = details.Duration;
            Distance = details.Distance;
            Shape = details.Shape;
            Instructions = details.Instructions;
        }

        /// <summary>
        ///     The duration required to complete the path.
        /// </summary>
        public Int32 Duration { get; private set; }

        /// <summary>
        ///     The distance that the path covers.
        /// </summary>
        public Int32 Distance { get; private set; }

        /// <summary>
        ///     The series of points that describes the path
        /// </summary>
        public ICollection<String> Shape { get; private set; }

        /// <summary>
        ///     The directional instructions for each manouvre throughout the path.
        /// </summary>
        public ICollection<Directions> Instructions { get; private set; }
    }
}
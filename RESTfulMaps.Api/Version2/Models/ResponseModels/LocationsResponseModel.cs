﻿using System;

namespace RESTfulMapsGateway.Api.Version2.Models.ResponseModels
{
    /// <summary>
    ///     The Locations response model.
    /// </summary>
    public class LocationsResponseModel
    {
        public LocationsResponseModel(string point, string address)
        {
            Point = point;
            Address = address;
        }

        /// <summary>
        ///     The point of the queried location.
        /// </summary>
        public String Point { get; private set; }


        /// <summary>
        ///     The address string describing the query point.
        /// </summary>
        public String Address { get; private set; }
    }
}
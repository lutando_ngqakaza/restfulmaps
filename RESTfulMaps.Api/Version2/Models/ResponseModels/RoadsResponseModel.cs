﻿using System;

namespace RESTfulMapsGateway.Api.Version2.Models.ResponseModels
{
    /// <summary>
    ///     The Roads response model.
    /// </summary>
    public class RoadsResponseModel
    {
        public RoadsResponseModel(string point, double speedLimit, string address)
        {
            Point = point;
            SpeedLimit = speedLimit;
            Address = address;
        }

        /// <summary>
        ///     The geographically corrected point of the query.
        /// </summary>
        public String Point { get; private set; }

        /// <summary>
        ///     The speedlimit of the road
        /// </summary>
        public Double SpeedLimit { get; private set; }

        /// <summary>
        ///     The address of the road.
        /// </summary>
        public String Address { get; private set; }
    }
}
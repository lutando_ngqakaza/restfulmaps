﻿using System;

namespace RESTfulMapsGateway.Api.Version2.Models.Data
{
    public class Manoeuvre
    {
        public Manoeuvre(string instruction, int duration, int distance)
        {
            Instruction = instruction;
            Duration = duration;
            Distance = distance;
        }

        public String Instruction { get; set; }

        public Int32 Duration { get; set; }

        public Int32 Distance { get; set; }
    }
}
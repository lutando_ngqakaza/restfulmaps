﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace RESTfulMapsGateway.Api.Validation
{
    public sealed class IsLongitudeValidationAttribute : ValidationAttribute
    {
        public IsLongitudeValidationAttribute([CallerMemberName] string propertyName = null)
            : base(String.Format("{0} is not numerical", propertyName))
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; private set; }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }


            return true;
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using RESTfulMapsGateway.Domain.Models.Enums;

namespace RESTfulMapsGateway.Api.Validation
{
    public sealed class MultiModelPropertyRequiredAttribute : RequiredAttribute
    {
        public MultiModelPropertyRequiredAttribute(InputModelType modelType,
            [CallerMemberName] string propertyName = null)
        {
            ModelType = modelType;

            PropertyName = propertyName;

            AllowEmptyStrings = false;

            ErrorMessage = String.Format("{0} required", PropertyName);
        }

        public InputModelType ModelType { get; private set; }

        public bool Valid { get; private set; }
        public string PropertyName { get; private set; }

        public override bool IsValid(object value)
        {
            Valid = base.IsValid(value);

            return true;
        }
    }
}
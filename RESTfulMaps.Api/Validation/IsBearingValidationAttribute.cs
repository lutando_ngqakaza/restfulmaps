﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace RESTfulMapsGateway.Api.Validation
{
    public sealed class IsBearingValidationAttribute : ValidationAttribute
    {
        public IsBearingValidationAttribute([CallerMemberName] string propertyName = null)
            : base(String.Format("{0} is not numerical", propertyName))
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; private set; }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var bearing = (int) value;

            if (bearing > 360 || bearing < -360)
            {
                return false;
            }


            return true;
        }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace RESTfulMapsGateway.Api.Validation
{
    public sealed class IsNumberValidationAttribute : ValidationAttribute
    {
        public IsNumberValidationAttribute([CallerMemberName] string propertyName = null)
            : base(String.Format("{0} is not numerical", propertyName))
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; private set; }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            Match match = Regex.Match(value.ToString(), @"^\d+$");

            return match.Success;
        }
    }
}
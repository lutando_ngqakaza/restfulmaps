﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace RESTfulMapsGateway.Api.Validation
{
    public sealed class PropertyRequiredAttribute : RequiredAttribute
    {
        public PropertyRequiredAttribute([CallerMemberName] string propertyName = null)
        {
            PropertyName = propertyName;

            AllowEmptyStrings = false;

            ErrorMessage = String.Format("{0} required", PropertyName);
        }

        public string PropertyName { get; private set; }
    }
}
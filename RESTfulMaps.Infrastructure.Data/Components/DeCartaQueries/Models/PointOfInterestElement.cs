﻿using System;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class PointOfInterestElement
    {
        public String Type { get; set; }

        public int Id { get; set; }

        public float Score { get; set; }

        public DeCartaPointOfInterest Poi { get; set; }

        public Address Address { get; set; }

        public DeCartaPosition Position { get; set; }
    }
}
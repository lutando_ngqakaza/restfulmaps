﻿namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class DeCartaPosition
    {
        public string Lat { get; set; }
        public string Lon { get; set; }
    }
}
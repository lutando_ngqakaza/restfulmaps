﻿using System;
using System.Collections.Generic;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class Address
    {
        public Locale Locale { get; set; }

        public string BuildingNumber { get; set; }

        public string StreetNumber { get; set; }

        public string Landmark { get; set; }

        public string Street { get; set; }
        public string StreetName { get; set; }
        public string StreetNameAndNumber { get; set; }

        public string SpeedLimit { get; set; }

        public string CountryCode { get; set; }

        public string CountrySubdivision { get; set; }

        public string CountrySecondarySubdivision { get; set; }

        public string CountryTertiarySubdivision { get; set; }

        public string Municipality { get; set; }

        public string PostalCode { get; set; }

        public string MunicipalitySubdivision { get; set; }

        public string Type { get; set; }

        public string FreeformAddress { get; set; }

        public string GetCompact()
        {
            if (!String.IsNullOrEmpty(FreeformAddress))
            {
                return FreeformAddress.Replace("\n", "");
            }

            var resultSnippets = new List<string>();

            if (!String.IsNullOrEmpty(BuildingNumber))
            {
                resultSnippets.Add(BuildingNumber);
            }
            if (!String.IsNullOrEmpty(StreetNumber) && !String.IsNullOrEmpty(Street))
            {
                resultSnippets.Add(StreetNumber + " " + Street);
            }
            else if (!String.IsNullOrEmpty(StreetNameAndNumber))
            {
                resultSnippets.Add(StreetNameAndNumber);
            }
            if (!String.IsNullOrEmpty(StreetName))
            {
                resultSnippets.Add(StreetName);
            }

            return String.Join(", ", resultSnippets);
        }

        public override string ToString()
        {
            if (!String.IsNullOrEmpty(FreeformAddress))
            {
                return FreeformAddress.Replace("\n", "");
            }

            var resultSnippets = new List<string>();

            if (!String.IsNullOrEmpty(BuildingNumber))
            {
                resultSnippets.Add(BuildingNumber);
            }
            if (!String.IsNullOrEmpty(StreetNumber) && !String.IsNullOrEmpty(Street))
            {
                resultSnippets.Add(StreetNumber + " " + Street);
            }
            else if (!String.IsNullOrEmpty(StreetNameAndNumber))
            {
                resultSnippets.Add(StreetNameAndNumber);
            }
            if (!String.IsNullOrEmpty(StreetName))
            {
                resultSnippets.Add(StreetName);
            }
            if (!String.IsNullOrEmpty(Municipality))
            {
                resultSnippets.Add(Municipality);
            }
            if (!String.IsNullOrEmpty(CountrySecondarySubdivision))
            {
                resultSnippets.Add(CountrySecondarySubdivision);
            }
            if (!String.IsNullOrEmpty(PostalCode))
            {
                resultSnippets.Add(PostalCode);
            }
            if (!String.IsNullOrEmpty(CountryCode))
            {
                resultSnippets.Add(CountryCode);
            }

            return String.Join(", ", resultSnippets);
        }
    }
}
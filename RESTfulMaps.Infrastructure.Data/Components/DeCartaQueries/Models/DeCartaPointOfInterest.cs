﻿using System.Collections.Generic;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class DeCartaPointOfInterest
    {
        public string Name { get; set; }

        public IEnumerable<string> Categories { get; set; }
    }
}
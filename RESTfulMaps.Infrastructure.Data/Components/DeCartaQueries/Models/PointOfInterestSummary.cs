﻿using System;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class PointOfInterestSummary
    {
        public String Query { get; set; }

        public string QueryType { get; set; }

        public int QueryTime { get; set; }

        public int NumResults { get; set; }

        public int TotalResults { get; set; }
    }
}
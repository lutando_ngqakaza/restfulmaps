﻿using System.Collections.Generic;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class PointOfInterestResult
    {
        public PointOfInterestSummary Summary { get; set; }

        public IEnumerable<PointOfInterestElement> Results { get; set; }
    }
}
﻿using System;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models
{
    public class Locale
    {
        public String Language { get; set; }

        public String Country { get; set; }
    }
}
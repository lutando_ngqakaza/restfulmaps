﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries.Models;
using Newtonsoft.Json;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DeCartaQueries
{
    public class DeCartaPointsOfInterestService : IPointsOfInterestService
    {
        private static readonly string urlMapIt = ConfigurationManager.ConnectionStrings["MapItWebApi"].ToString();


        public IEnumerable<PointOfInterest> GetPointsOfInterest(String searchText, int limit)
        {
            string key = ConfigurationManager.AppSettings["MapItClientPassword"];

            string restUri = urlMapIt + key
                             + "/poi/"
                             + HttpUtility.UrlEncode(searchText)
                             + ".json";

            string criteria = "limit=" + limit;
            restUri += "?" + criteria;

            return GetPoIntsOfInterest(restUri);
        }

        public IEnumerable<PointOfInterest> GetPointsOfInterest(String searchText, Coordinate searchCoordinate,
            int limit)
        {
            string key = ConfigurationManager.AppSettings["MapItClientPassword"];

            string restUri = urlMapIt + key
                             + "/poi/"
                             + HttpUtility.UrlEncode(searchText)
                             + ".json";

            string latitude = searchCoordinate.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string longitude = searchCoordinate.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);

            string criteria = "lat=" + latitude + "&lon=" + longitude + "&limit=" + limit;
            restUri += "?" + criteria;

            return GetPoIntsOfInterest(restUri);
        }

        private IEnumerable<PointOfInterest> GetPoIntsOfInterest(string uriString)
        {
            var httpWebRequest = (HttpWebRequest) WebRequest.Create(uriString);

            httpWebRequest.Method = "GET";

            var response = (HttpWebResponse) httpWebRequest.GetResponse();

            Stream responseStream = response.GetResponseStream();
            string data;

            using (var reader = new StreamReader(responseStream))
            {
                data = reader.ReadToEnd();
            }

            var RouteResult = JsonConvert.DeserializeObject<PointOfInterestResult>(data);

            return RouteResult.Results == null
                ? new List<PointOfInterest>()
                : RouteResult.Results.Select(BuildPointOfInterestFromElement);
        }


        private String ToUtf8(String s)
        {
            byte[] bytes = Encoding.Default.GetBytes(s);
            return Encoding.UTF8.GetString(bytes);
        }

        private PointOfInterest BuildPointOfInterestFromElement(PointOfInterestElement e)
        {
            string name = e.Poi.Name;
            //var description = e.Address.ToString();
            string description = ToUtf8(e.Address.ToString());
            IEnumerable<string> categories = e.Poi.Categories;
            double latitude = Convert.ToDouble(e.Position.Lat, CultureInfo.InvariantCulture);
            double longitude = Convert.ToDouble(e.Position.Lon, CultureInfo.InvariantCulture);
            var location = new Coordinate(latitude, longitude);
            string address = e.Address.ToString();

            return new PointOfInterest(name, description, categories, location, address);
        }
    }
}
﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace RESTfulMapsGateway.Infrastructure.Data.Components
{
    public class StateObject
    {

        public const int BufferSize = 256;

        public StateObject()
        {
            WorkSocket = null;
            Buffer = new byte[BufferSize];
            SB = new StringBuilder();
        }



        public Socket WorkSocket { get; set; }
        public byte[] Buffer { get; set; }

        public StringBuilder SB { get; set; }
    }

    public class TcpClient : IDisposable
    {
        private static String response = String.Empty;
        private readonly String address;
        private readonly ManualResetEvent connectDone;
        private readonly int port;
        private readonly ManualResetEvent receiveDone;
        private readonly ManualResetEvent sendDone;




        public TcpClient(String address, int port)
        {
            this.address = address;
            this.port = port;
            connectDone = new ManualResetEvent(false);
            sendDone = new ManualResetEvent(false);
            receiveDone = new ManualResetEvent(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public String SendMessage(String message)
        {
            try
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(address);

                IPAddress ipAddress = ipHostInfo.AddressList[0];


                foreach (IPAddress ip in ipHostInfo.AddressList)
                {
                    AddressFamily af = ip.AddressFamily;
                    if (af == AddressFamily.InterNetwork)
                    {
                        ipAddress = ip;
                        break;
                    }
                }

                var remoteEP = new IPEndPoint(ipAddress, port);

                var client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                client.BeginConnect(remoteEP, ConnectCallback, client);
                connectDone.WaitOne();

                Send(client, message);
                sendDone.WaitOne();

                Receive(client);
                receiveDone.WaitOne();

                client.Shutdown(SocketShutdown.Both);
                client.Close();

                return response;
            }
            catch (Exception e)
            {
                // Throw Appropriate Exception
                return e.ToString();
            }
        }

        

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                var client = (Socket) ar.AsyncState;

                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint);

                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void Receive(Socket client)
        {
            try
            {
                var state = new StateObject();
                state.WorkSocket = client;

                client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0,
                    ReceiveCallback, state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }



        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject) ar.AsyncState;
                Socket client = state.WorkSocket;

                // Read data from the remote device.
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    state.SB.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

                    // Get the rest of the data.
                    client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0,
                        ReceiveCallback, state);
                }
                else
                {
                    // All the data has arrived; put it in response.
                    if (state.SB.Length > 1)
                    {
                        response = state.SB.ToString();
                    }
                    // Signal that all bytes have been received.
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        

        private void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            client.BeginSend(byteData, 0, byteData.Length, 0,
                SendCallback, client);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                var client = (Socket) ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (connectDone != null)
                {
                    connectDone.Dispose();
                }

                if (sendDone != null)
                {
                    sendDone.Dispose();
                }

                if (receiveDone != null)
                {
                    receiveDone.Dispose();
                }
            }
        }
    }
}
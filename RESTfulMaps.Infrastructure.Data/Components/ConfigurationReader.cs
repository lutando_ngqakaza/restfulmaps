﻿using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Configuration;

namespace RESTfulMapsGateway.Infrastructure.Data.Components
{
    public class ConfigurationReader : IConfigurationReader
    {
        public string ReadConfigurationValue(string key)
        {
            return CachedConfigurationManager.Instance.GetSetting(key);
        }
    }
}
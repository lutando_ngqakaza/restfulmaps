﻿using System;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;

namespace RESTfulMapsGateway.Infrastructure.Data.Components
{
    public class DdsClient
    {
        public DdsClient(TcpClient tcpClient, String query)
        {
            TcpClient = tcpClient;
            Query = query;
        }

        private TcpClient TcpClient { get; set; }
        private String Query { get; set; }

        //IEnumerable<String> Queries { get; set; }

        [LoggingMethod]
        public String SendQuery()
        {
            string message = TcpClient.SendMessage(Query);
            return message;
        }

        /*public IEnumerable<String> SendQueries()
        {
            var messages = TcpClient.SendMessages(Queries);
            return messages;
        }*/

        /*public DdsClient(TcpClient tcpClient, IEnumerable<String> queries)
        {
            TcpClient = tcpClient;
            Queries = queries;
        }*/
    }
}
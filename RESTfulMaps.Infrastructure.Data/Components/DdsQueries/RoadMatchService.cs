﻿using System;
using System.Globalization;
using System.Reflection;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using Microsoft.Azure;
using lutz.Logging.Loggers;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class RoadMatchService : IRoadMatchService
    {
        public RoadDetails GetRoadMatch(Coordinate queryLocation)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}", queryLocation));

            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");


            //change to appropriate culture
            string qlat = queryLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string qlong = queryLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);


            string query = String.Format("S2LN|%LL={0},{1}|%SPEEDLIMIT%SN%GZ||", qlat, qlong);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);


            string[] splitResult = result.Split('%');
            decimal speedLimit = 0;

            string streetName = "";
            string suburb = "";
            string city = "";
            string province = "";
            Coordinate snapLocation = null;

            foreach (string element in splitResult)
            {
                string temp = element.Trim('|');

                if (temp.Contains("LL="))
                {
                    temp = temp.Substring(3);
                    snapLocation = new Coordinate(temp);
                }
                if (temp.Contains("G1="))
                {
                    temp = temp.Substring(3);
                    province = temp;
                }
                if (temp.Contains("G4="))
                {
                    temp = temp.Substring(3);
                    city = temp;
                }
                if (temp.Contains("G5="))
                {
                    temp = temp.Substring(3);
                    suburb = temp;
                }
                if (temp.Contains("SN="))
                {
                    int endIndex = temp.IndexOf(':');
                    streetName = temp.Substring(3, temp.Length - endIndex - 4);
                }
                if (temp.Contains("SPEEDLIMIT="))
                {
                    string parseString = temp.Substring(11, temp.Length - temp.IndexOf("KPH") + 1);
                    bool canParseSpeed = Decimal.TryParse(parseString, NumberStyles.Any, CultureInfo.InvariantCulture,
                        out speedLimit);

                    if (!canParseSpeed)
                    {
                        throw new InvalidCastException("DDS result for RoadMatch could not be parsed");
                    }
                }
            }

            return new RoadDetails(province, city, suburb, streetName, speedLimit, snapLocation);
        }

        public RoadDetails GetRoadMatch(Coordinate queryLocation, double bearing)
        {
            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");
            //change to appropriate culture
            string qlat = queryLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string qlong = queryLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string angle = bearing.ToString(CultureInfo.InvariantCulture.NumberFormat);

            string query = String.Format("S2LN|%LL={0},{1}%ANG={2}|%SPEEDLIMIT%SN%GZ||", qlat, qlong, angle);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);


            string[] splitResult = result.Split('%');
            decimal speedLimit = 0;

            string streetName = "";
            string suburb = "";
            string city = "";
            string province = "";
            Coordinate snapLocation = null;

            foreach (string element in splitResult)
            {
                string temp = element.Trim('|');

                if (temp.Contains("LL="))
                {
                    temp = temp.Substring(3);
                    snapLocation = new Coordinate(temp);
                }
                if (temp.Contains("G1="))
                {
                    temp = temp.Substring(3);
                    province = temp;
                }
                if (temp.Contains("G4="))
                {
                    temp = temp.Substring(3);
                    city = temp;
                }
                if (temp.Contains("G5="))
                {
                    temp = temp.Substring(3);
                    suburb = temp;
                }
                if (temp.Contains("SN="))
                {
                    int endIndex = temp.IndexOf(':');
                    streetName = temp.Substring(3, temp.Length - endIndex - 4);
                }
                if (temp.Contains("SPEEDLIMIT="))
                {
                    string parseString = temp.Substring(11, temp.Length - temp.IndexOf("KPH") + 1);
                    bool canParseSpeed = Decimal.TryParse(parseString, NumberStyles.Any, CultureInfo.InvariantCulture,
                        out speedLimit);

                    if (!canParseSpeed)
                    {
                        throw new InvalidCastException("DDS result for RoadMatch could not be parsed");
                    }
                }
            }

            return new RoadDetails(province, city, suburb, streetName, speedLimit, snapLocation);
        }
    }
}
﻿using System;
using System.Globalization;
using System.Reflection;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.ValueObjects;
using Microsoft.Azure;
using lutz.Logging.Loggers;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class ReverseGeocodeService : IReverseGeocodeService
    {
        public RoadDetails GetReverseGeocode(Coordinate queryLocation)
        {
            MethodBase info = MethodBase.GetCurrentMethod();
            string logContext = String.Format("{0}.{1}", info.ReflectedType.FullName, info.Name);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(logContext, String.Format("query: {0}", queryLocation));

            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");


            //change to appropriate culture
            string qlat = queryLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string qlong = queryLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);


            string query =
                String.Format(
                    "S2LN|%LL={0},{1}%CUST=lutzAdministrator|%LL%RAD=M1000%SN%ADDR%CAT=7%GZ%PC%AC%S1%M=10%ADDRPT" +
                    "%INTERPADDR%EXTIME%DS=tomtom-zaf%DATAVENDOR%CHARACTERENCODING=iso-8859-1||", qlat, qlong);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);

            string[] splitResult = result.Split('%');


            string streetName = "";
            string suburb = "";
            string city = "";
            string province = "";
            Coordinate snapLocation = null;
            string streetNumber = "";

            foreach (string element in splitResult)
            {
                string temp = element.Trim('|');

                if (temp.Contains("LL="))
                {
                    temp = temp.Substring(3);
                    snapLocation = new Coordinate(temp);
                }
                if (temp.Contains("G1="))
                {
                    temp = temp.Substring(3);
                    province = temp;
                }
                if (temp.Contains("G4="))
                {
                    temp = temp.Substring(3);
                    city = temp.Trim();
                }
                if (temp.Contains("G5="))
                {
                    temp = temp.Substring(3);
                    suburb = temp.Trim();
                }
                if (temp.Contains("SN="))
                {
                    int endIndex = temp.IndexOf(':');
                    streetName = temp.Substring(3, temp.Length - endIndex - 4).Trim();
                }
                if (temp.Contains("INTERPADDR="))
                {
                    temp = temp.Substring(11);
                    streetNumber = temp.Trim();
                }
            }
            string streetAddress = String.Format("{0} {1}", streetNumber, streetName).Trim();
            return new RoadDetails(province, city, suburb, streetAddress, 0, snapLocation);
        }
    }
}
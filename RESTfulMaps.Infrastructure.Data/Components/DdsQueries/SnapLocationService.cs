﻿using System;
using System.Globalization;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using Microsoft.Azure;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class SnapLocationService : ISnapLocationService
    {
        public Coordinate GetSnappedLocation(Coordinate queryLocation)
        {
            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");

            //change to appropriate culture
            string qlat = queryLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string qlong = queryLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);


            string query = String.Format("S2LN|%LL={0},{1}|||", qlat, qlong);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);

            int llPosition = result.IndexOf("LL=") + 3;
            result = result.Substring(llPosition).Trim('|');

            string[] parsedResult = result.Split(',');
            double rlat = 0.0;
            double rlong = 0.0;
            bool latParsed = Double.TryParse(parsedResult[0], NumberStyles.Any, CultureInfo.InvariantCulture, out rlat);
            bool longParsed = Double.TryParse(parsedResult[1], NumberStyles.Any, CultureInfo.InvariantCulture, out rlong);

            if (!longParsed || !latParsed)
            {
                throw new InvalidCastException("DDS result for RoadMatch could not be parsed");
            }


            var snapLocation = new Coordinate(rlat, rlong);


            return snapLocation;
        }
    }
}
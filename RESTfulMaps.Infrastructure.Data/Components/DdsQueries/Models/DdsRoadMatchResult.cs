﻿using System;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries.Models
{
    public class DdsRoadMatchResult
    {
        public DdsRoadMatchResult(string streetName, decimal speedLimit, Coordinate location)
        {
            StreetName = streetName;
            SpeedLimit = speedLimit;
            Location = location;
        }

        public String StreetName { get; set; }

        public Decimal SpeedLimit { get; set; }

        public Coordinate Location { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using RESTfulMapsGateway.Infrastructure.Data.Modules.Logging;
using Microsoft.Azure;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class RailRoutingService : IRailRoutingService
    {
        [LoggingMethod]
        public IEnumerable<Coordinate> GetRailRoute(Coordinate startingLocation, Coordinate destinationLocation)
        {
            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");

            string slat = startingLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string slong = startingLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlat = destinationLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlong = destinationLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);


            string query = String.Format("ROUT|%OLL={0},{1}%DLL={2},{3}|%TURN%DIST=M%LANECON%LL%DS=Railways-data||",
                slat, slong, dlat, dlong);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);
            int vrPosition = result.IndexOf("VR=") + 3;
            result = result.Substring(vrPosition).Trim('|');

            string[] parsedResult = result.Split(',');
            var path = new List<Coordinate>();

            int resultSize = int.Parse(parsedResult[0]);

            for (int k = 0; k < resultSize*2; k = k + 2)
            {
                path.Add(new Coordinate(double.Parse(parsedResult[k + 1])/100000,
                    double.Parse(parsedResult[k + 2])/100000));
            }
            for (int k = 0; k < resultSize - 1; k++)
            {
                path[k + 1] = new Coordinate(path[k + 1].Latitude + path[k].Latitude,
                    path[k + 1].Longitude + path[k].Longitude);
            }

            return path;
        }

        public RouteDetails GetRailRouteDetails(Coordinate startingLocation, Coordinate destinationLocation)
        {
            //throw new NotImplementedException();
            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");
            //change to appropriate culture
            string slat = startingLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string slong = startingLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlat = destinationLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlong = destinationLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);


            string query =
                String.Format("RTXT|%OLL={0},{1}%DLL={2},{3}|%TURN%DIST=M%LANECON%LL%DS=Railways-data%VR%TTIME||", slat,
                    slong, dlat, dlong);
            var client = new TcpClient(address, port);
            string result = client.SendMessage(query);

            int totalTime = 0;
            int totalDistance = 0;
            ICollection<Coordinate> shape = null;
            string[] splitResult = result.Split('%');

            foreach (string element in splitResult)
            {
                string temp = element.Trim('|');

                if (temp.Contains("TTIME="))
                {
                    temp = temp.Substring(6);
                    temp = temp.Substring(0, temp.IndexOf(" sec"));
                    totalTime = Int32.Parse(temp.Split('.')[0]);
                }
                if (temp.Contains("DIST=M"))
                {
                    temp = temp.Substring(6);
                    totalDistance = Int32.Parse(temp.Split('.')[0]);
                }
                if (temp.Contains("VR="))
                {
                    shape = GetCoordinates(temp);
                }
            }

            return new RouteDetails(shape, totalTime, totalDistance);
        }

        private ICollection<Coordinate> GetCoordinates(string vectorString)
        {
            int vectorPosition = vectorString.IndexOf("VR=") + 3;
            string result = vectorString.Substring(vectorPosition).Trim('|');

            string[] parsedResult = result.Split(',');
            var shape = new List<Coordinate>();

            int resultSize = int.Parse(parsedResult[0]);

            for (int k = 0; k < resultSize*2; k = k + 2)
            {
                shape.Add(new Coordinate(double.Parse(parsedResult[k + 1])/100000,
                    double.Parse(parsedResult[k + 2])/100000));
            }
            for (int k = 0; k < resultSize - 1; k++)
            {
                shape[k + 1] = new Coordinate(shape[k + 1].Latitude + shape[k].Latitude,
                    shape[k + 1].Longitude + shape[k].Longitude);
                //path[k + 1] = path[k + 1] + path[k];
            }

            return shape;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.Enums;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class PathingService : IPathingService
    {
        public PathingService(IRailRoutingService railRoutingService, IRoadRoutingService roadRoutingService)
        {
            RailRoutingService = railRoutingService;
            RoadRoutingService = roadRoutingService;
        }

        private IRailRoutingService RailRoutingService { get; set; }

        private IRoadRoutingService RoadRoutingService { get; set; }

        public RouteDetails GetRoute(ICollection<Coordinate> points, TransitMode mode)
        {
            //string ddsMode;
            if (mode == TransitMode.Rail)
            {
                RouteDetails result = RailRoutingService.GetRailRouteDetails(points.ElementAt(0),
                    points.ElementAt(points.Count() - 1));

                return result;
            }
            if (mode == TransitMode.Driving)
            {
                RouteDetails result = RoadRoutingService.GetCarRouteDetails(points);
                return result;
            }
            if (mode == TransitMode.Walking)
            {
                RouteDetails result = RoadRoutingService.GetWalkingRouteDetails(points);
                return result;
            }
            throw new ArgumentException(String.Format("invalid transit mode {0}", mode));

            //dunno how to properly do this
            //throw new Exception("No result found for query");
        }

       
    }
}
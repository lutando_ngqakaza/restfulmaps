﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using RESTfulMapsGateway.Domain.Interfaces.Components;
using RESTfulMapsGateway.Domain.Models.RESTfulMapsModels;
using Microsoft.Azure;
using lutz.ValueObjects;

namespace RESTfulMapsGateway.Infrastructure.Data.Components.DdsQueries
{
    public class RoadRoutingService : IRoadRoutingService
    {
        [SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
        public RouteDetails GetCarRouteDetails(ICollection<Coordinate> points)
        {
            Coordinate startingLocation = points.ElementAt(0);
            Coordinate destinationLocation = points.ElementAt(points.Count - 1);

            string slat = startingLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string slong = startingLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlat = destinationLocation.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            string dlong = destinationLocation.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);

            var waypoints = new List<Coordinate>(points);
            waypoints = waypoints.GetRange(1, waypoints.Count - 2);
            string waypointString = "";

            foreach (Coordinate waypoint in waypoints)
            {
                string wlat = waypoint.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
                string wlong = waypoint.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
                //%WP=%WDLL=-33.975402,18.453697%WOLL=-33.975402,18.453697
                waypointString = waypointString + String.Format("%WP=%WDLL={0},{1}%WOLL={0},{1}", wlat, wlong);
            }

            string query = String.Format(
                "RTXT|%OLL={0},{1}%DLL={2},{3}{4}%CUST=lutzAdministrator|" +
                "%FORK=F%EXITCNT%AC=121%S2%PX%WP%RS=Fast%RULES=rules/maneuver-rules.xml%OHED%DHED%TEXT%LL%DLL%DSTR%FTANG%TURN%TX" +
                "%DIST=M%TIME%N%SN%SGNB%EXIT%VR%ROADUSE%RG%LLMIN%LLMAX%TTIME%TDIST=M%DPRTMNVR%TIMEOUT=20000%OANG%DANG%TANG%EXTIME||",
                slat, slong, dlat, dlong, waypointString
                );

            string ddsResult = RequestDds(query);

            if (!ddsResult.Contains("S=OK"))
            {
                //create dds exception
                throw new Exception("Query to the DDS yielded no result");
            }
            int totalTime = 0;
            int totalDistance = 0;
            ICollection<Coordinate> shape = null;
            var instructions = new List<Directions>();
            string[] splitResult = ddsResult.Trim('|').Split('|');

            foreach (string siblingResult in splitResult)
            {
                string[] childResults = siblingResult.Split('%');

                //parent
                if (siblingResult.Contains("TTIME="))
                {
                    foreach (string item in childResults)
                    {
                        string temp = item.Trim('|');

                        if (temp.Contains("TTIME="))
                        {
                            temp = temp.Substring(6);
                            temp = temp.Substring(0, temp.IndexOf(" sec"));
                            totalTime = Int32.Parse(temp.Split('.')[0]);
                        }
                        if (temp.Contains("TDIST=M"))
                        {
                            temp = temp.Substring(7);
                            totalDistance = Int32.Parse(temp.Split('.')[0]);
                        }
                        if (temp.Contains("VR="))
                        {
                            shape = GetCoordinates(temp);
                        }
                    }
                }
                //child
                if (siblingResult.Contains("TEXT="))
                {
                    int time = 0;
                    int distance = 0;
                    ICollection<Coordinate> directionShape = null;
                    string instruction = String.Empty;
                    string text = String.Empty;
                    string streetName = String.Empty;
                    foreach (string item in childResults)
                    {
                        string temp = item.Trim('|');


                        if (temp.Contains("TIME="))
                        {
                            temp = temp.Substring(5);
                            temp = temp.Substring(0, temp.IndexOf(" sec"));
                            time = Int32.Parse(temp.Split('.')[0]);
                        }
                        if (temp.Contains("DIST=M"))
                        {
                            temp = temp.Substring(6);
                            distance = Int32.Parse(temp.Split('.')[0]);
                        }
                        if (temp.Contains("VR="))
                        {
                            directionShape = GetCoordinates(temp);
                        }
                        if (temp.Contains("TEXT="))
                        {
                            temp = temp.Substring(5);
                            text = temp;
                        }
                        if (temp.Contains("DSTR="))
                        {
                            temp = temp.Substring(5);
                            streetName = temp;
                            if (streetName.Contains("\\:RN"))
                            {
                                streetName = streetName.Substring(0, streetName.IndexOf("\\"));
                            }
                        }
                    }
                    instruction = String.Format("{0} {1}", text, streetName).Trim();
                    var direction = new Directions(instruction, time, distance, directionShape);
                    instructions.Add(direction);
                }
            }
            return new RouteDetails(shape, instructions, totalTime, totalDistance);
        }

        public RouteDetails GetWalkingRouteDetails(ICollection<Coordinate> points)
        {
            throw new NotImplementedException();
        }

        public String RequestDds(string ddsQuery)
        {
            int port = int.Parse(CloudConfigurationManager.GetSetting("Dds.Port"));
            string address = CloudConfigurationManager.GetSetting("Dds.Url");
            var client = new TcpClient(address, port);
            string result = client.SendMessage(ddsQuery);
            return result;
        }

        private ICollection<Coordinate> GetCoordinates(string vectorString)
        {
            int vectorPosition = vectorString.IndexOf("VR=") + 3;
            string result = vectorString.Substring(vectorPosition).Trim('|');

            string[] parsedResult = result.Split(',');
            var shape = new List<Coordinate>();

            int resultSize = int.Parse(parsedResult[0]);

            for (int k = 0; k < resultSize*2; k = k + 2)
            {
                shape.Add(new Coordinate(double.Parse(parsedResult[k + 1])/100000,
                    double.Parse(parsedResult[k + 2])/100000));
            }
            for (int k = 0; k < resultSize - 1; k++)
            {
                shape[k + 1] = new Coordinate(shape[k + 1].Latitude + shape[k].Latitude,
                    shape[k + 1].Longitude + shape[k].Longitude);
                //path[k + 1] = path[k + 1] + path[k];
            }

            return shape;
        }
    }
}
﻿using System;
using PostSharp.Aspects;
using lutz.Logging.Loggers;

namespace RESTfulMapsGateway.Infrastructure.Data.Modules.Logging
{
    [Serializable]
    public sealed class LoggingMethodAttribute : OnMethodBoundaryAspect
    {
        public string GetContext(MethodExecutionArgs args)
        {
            string context = String.Empty;
            if (args.Instance == null)
            {
                context = args.Method.Name;
            }
            else
            {
                context = args.Instance.GetType() + "." + args.Method.Name;
            }

            return context;
        }

        public override void OnEntry(MethodExecutionArgs args)
        {
            string context = GetContext(args);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogInfo(context, String.Format("Method called"));
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            string context = GetContext(args);
            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogTrace(context, "Method successfully executed.");
        }

        public override void OnException(MethodExecutionArgs args)
        {
            string context = GetContext(args);

            ApplicationLogger logger = ApplicationLogger.GetLogger("Application");
            logger.LogFatal(context, String.Format("Method failed with exception."), args.Exception);
        }
    }
}
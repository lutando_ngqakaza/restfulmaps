﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using Microsoft.Azure;

namespace RESTfulMapsGateway.Infrastructure.Data.Modules.Configuration
{
    public class CachedConfigurationManager
    {
        private CachedConfigurationManager()
        {
            Settings = new Dictionary<string, string>();
        }

        private Dictionary<string, string> Settings { get; set; }

        public static CachedConfigurationManager Instance
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    Debug.WriteLine(
                        "CachedConfigurationSettings:  HttpContext Disposed. Creating new caching instance...");
                    return new CachedConfigurationManager();
                }

                return
                    (HttpContext.Current.Items["CachedConfigurationManager"] ??
                     (HttpContext.Current.Items["CachedConfigurationManager"] = new CachedConfigurationManager())) as
                        CachedConfigurationManager;
            }
        }

        public string GetSetting(string key)
        {
            if (Settings.ContainsKey(key))
            {
                Debug.WriteLine("CachedConfigurationSettings:  Getting " + key + " from cache...");
                return Settings[key];
            }
            Debug.WriteLine("CachedConfigurationSettings:  Restoring " + key + " from config...");
            return RestoreSetting(key);
        }

        public T GetSetting<T>(string key)
        {
            string value = GetSetting(key);

            return (T) Convert.ChangeType(value, typeof (T));
        }

        private string RestoreSetting(string key)
        {
            string value = CloudConfigurationManager.GetSetting(key);

            if (Settings.ContainsKey(key))
            {
                Settings[key] = value;
            }
            else
            {
                Settings.Add(key, value);
            }

            return value;
        }
    }
}